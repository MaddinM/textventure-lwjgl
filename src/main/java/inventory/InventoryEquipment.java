package inventory;

import main.Global;
import org.newdawn.slick.Graphics;

public class InventoryEquipment {
	private int x;
	private int y;

	public InventoryEquipment(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void render(Graphics g) {
		g.setColor(Global.getGrey(226));
		Global.drawSpacedString(g, "+-+", x, y);
		Global.drawSpacedString(g, "| |", x, y + 1);
		Global.drawSpacedString(g, "+-+", x, y + 2);
	}
}
