package inventory;

import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import save.PlayerData;

public class InventoryBackground {
	public void render(Graphics g) {
		g.setColor(Color.black);
		Global.drawSpacedString(g, "Name", 33, 1);
		Global.drawSpacedString(g, PlayerData.NAME, 20, 1);

		g.setColor(Global.getGrey(235));
		for (int i = 0; i < 12; i++) {
			Global.drawSpacedString(g, Global.charBar(29, '-'), 33, 3 + i * 2);
		}

		for (int i = 0; i < 20; i++) {
			Global.drawSpacedString(g, "#", 62, 3 + i);
		}
	}
}
