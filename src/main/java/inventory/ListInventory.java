package inventory;

import load.ScrollBar;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;
import save.InventoryData;

public class ListInventory {
	private InventoryButton[] buttons = new InventoryButton[InventoryData.size()];
	private ScrollBar scrollbar;
	private int scroll;

	public ListInventory() {
		scrollbar = new ScrollBar(62, 3, 20, 10, InventoryData.size(), Color.gray);

		for (int i = 0; i < InventoryData.size(); i++) {
			buttons[i] = new InventoryButton(InventoryData.get(i).getName(), 33, 4 + i * 2 + scroll);
		}
	}

	public void render(Graphics g) {
		scrollbar.render(g);

		g.setColor(Color.gray);
		for (InventoryButton inventorybutton : buttons) {
			inventorybutton.render(g);
		}
	}

	public void update(GameContainer gc, StateBasedGame sbg) {
		int mousewheel = Mouse.getDWheel();
		scrollUpOrDown(mousewheel);

		for (InventoryButton inventorybutton : buttons) {
			inventorybutton.update(gc, sbg, scroll);
		}
	}

	private void scrollUpOrDown(int mousewheel) {
		if (InventoryData.size() > 10) {
			if (mousewheel > 0 && scroll < 0) {
				scroll += 2;
				scrollbar.moveScrollBarDown();
			}
			if (mousewheel < 0 && scroll > (InventoryData.size() - 10) * -2) {
				scroll -= 2;
				scrollbar.moveScrollBarUp();
			}
		}
	}
}
