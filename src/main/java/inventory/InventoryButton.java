package inventory;

import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.StateBasedGame;

public class InventoryButton {
	private String name;
	private int x;
	private int y;
	private int scroll;

	private boolean hover = false;

	public InventoryButton(String name, int x, int y) {
		this.name = name;
		this.x = x;
		this.y = y;
	}

	public void render(Graphics g) {
		if (hover) {
			g.setColor(Color.black);
		} else {
			g.setColor(Color.lightGray);
		}

		if (visible()) {
			Global.drawSpacedString(g, name, x, y + scroll);
		}
	}

	public void update(GameContainer gc, StateBasedGame sbg, int scroll) {
		this.scroll = scroll;

		Input input = gc.getInput();
		int mouseX = input.getMouseX();
		int mouseY = input.getMouseY();

		hover = (mouseX > Coords.x(x) && mouseX < Coords.x(x + name.length())) &&
				(mouseY > Coords.y(y + scroll) && mouseY < Coords.y(y + 1 + scroll));

		if (hover && input.isMouseButtonDown(0)) {

		}
	}

	private boolean visible() {
		return y + scroll >= 3 && y + scroll <= 23;
	}
}
