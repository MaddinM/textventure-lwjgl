package lootscreen;

import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class LootScreenTimerBar {
	private int x;
	private int y;
	public double time;
	private double maxTime;
	private int maxBarLength = Coords.maxX() / 3 * 2 - 2;
	private boolean stop;

	public LootScreenTimerBar(int x, int y, double time) {
		this.x = x;
		this.y = y;
		this.time = time;
		this.maxTime = time;
	}

	public void render(Graphics g) {
		g.setColor(Color.lightGray);
		Global.drawSpacedString(g, Global.charBar(maxBarLength, '#'), x, y);

		g.setColor(Color.black);
		Global.drawSpacedString(g, Global.charBar((currentBalkenLaenge()), '#'), x, y);
	}

	public void update(int delta) {
		if (time > 0 && !stop) {
			time -= delta;
		}
	}

	private int currentBalkenLaenge() {
		double prozent = time / maxTime;
		double anzahlBalken = prozent * maxBarLength;
		return (int) anzahlBalken;
	}

	public void stopTimer() {
		stop = true;
	}
}
