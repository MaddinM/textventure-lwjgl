package lootscreen;

import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class LootScreenBlock {
	private String letter;
	private int x;
	private int y;
	private int id;
	// TODO make all grey numbers contants
	private Color color = Global.getGrey(220);

	LootScreenBlock(String letter, int x, int y, int id) {
		this.letter = letter;
		this.x = x;
		this.y = y;
		this.id = id;
	}

	public void render(Graphics g) {
		g.setColor(color);
		g.drawString(letter, Coords.x(x), Coords.y(y));
	}
	
	public int getID() {
		return id;
	}

	void setLightColor() {
		color = Global.getGrey(220);
	}

	void setDarkColor() {
		color = Color.gray;
	}

	void setBlackColor() {
		color = Color.black;
	}

	public Color getColor() {
		return color;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	String getLetter() {
		return letter;
	}
}
