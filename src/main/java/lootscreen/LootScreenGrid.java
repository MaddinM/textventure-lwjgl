package lootscreen;

import main.Constants;
import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import java.util.ArrayList;

public class LootScreenGrid {
	private ArrayList<LootScreenBlock> lootblocklist = new ArrayList<LootScreenBlock>();

	public int length;
	public boolean clicked;

	private int mouseX;
	private int mouseY;
	private Color hell = Global.getGrey(220);

	private int lootScreenWidth = Coords.maxX() / 3 * 2 - 2;
	private int lootScreenHeight = Coords.maxY() - 3;

	public LootScreenGrid(int x, int y) {
		generateLootLetters(x, y);
	}

	public void update(GameContainer gc) {
		Input input = gc.getInput();
		mouseX = input.getMouseX();
		mouseY = input.getMouseY();

		if (input.isMouseButtonDown(0)) {
			clicked = true;
		}

		if (!clicked) {
			if (onLetters()) {
				colorMouseLetter(mouseOnLetterID());
				colorLetters(mouseOnLetterID());
			} else {
				resetColors();
			}
		} else {
			makeClickedBlocksBlack();
		}
	}

	public void render(Graphics g) {
		for (LootScreenBlock l : lootblocklist) {
			l.render(g);
		}

		Global.drawSpacedString(g, "" + countDarkBlocks(), 3, 3);
	}

	private void colorLetters(int id) {
		colorTop(id);
		colorBottom(id);
		colorLeft(id);
		colorRight(id);
	}

	private boolean hasTopLetter(int id) {
		return onLetters() && id % lootScreenHeight != 0 && lootblocklist.get(id - 1).getColor().equals(hell);
	}

	private boolean hasBottomLetter(int id) {
		return onLetters() && id % lootScreenHeight != lootScreenHeight - 1 && lootblocklist.get(id + 1).getColor().equals(hell);
	}

	private boolean hasLeftLetter(int id) {
		return onLetters() && id > lootScreenHeight && lootblocklist.get(id - lootScreenHeight).getColor().equals(hell);
	}

	private boolean hasRightLetter(int id) {
		return onLetters() && id < length - 1 - lootScreenHeight && lootblocklist.get(id + lootScreenHeight).getColor().equals(hell);
	}

	private void colorTop(int id) {
		if (hasTopLetter(id) && lootblocklist.get(id - 1).getLetter().equals(lootblocklist.get(id).getLetter())) {
			lootblocklist.get(id - 1).setDarkColor();
			colorTop(id - 1);
			colorLeft(id - 1);
			colorRight(id - 1);
		}
	}

	private void colorBottom(int id) {
		if (hasBottomLetter(id) && lootblocklist.get(id + 1).getLetter().equals(lootblocklist.get(id).getLetter())) {
			lootblocklist.get(id + 1).setDarkColor();
			colorBottom(id + 1);
			colorLeft(id + 1);
			colorRight(id + 1);
		}
	}

	private void colorLeft(int id) {
		if (hasLeftLetter(id) && lootblocklist.get(id - lootScreenHeight).getLetter().equals(lootblocklist.get(id).getLetter())) {
			lootblocklist.get(id - lootScreenHeight).setDarkColor();
			colorLeft(id - lootScreenHeight);
			colorTop(id - lootScreenHeight);
			colorBottom(id - lootScreenHeight);
		}
	}

	private void colorRight(int id) {
		if (hasRightLetter(id) && lootblocklist.get(id + lootScreenHeight).getLetter().equals(lootblocklist.get(id).getLetter())) {
			lootblocklist.get(id + lootScreenHeight).setDarkColor();
			colorRight(id + lootScreenHeight);
			colorTop(id + lootScreenHeight);
			colorBottom(id + lootScreenHeight);
		}
	}

	private void colorMouseLetter(int id) {
		for (LootScreenBlock l : lootblocklist) {
			if (l.getID() == id) {
				l.setDarkColor();
				continue;
			}
			l.setLightColor();
		}
	}

	private void generateLootLetters(int x, int y) {
		for (int i = 0; i < lootScreenWidth; i++) {
			for (int j = 0; j < lootScreenHeight; j++) {
				String randomletters = "MTRW";
				lootblocklist.add(new LootScreenBlock(Global.randomLetter(randomletters), x + i, y + j, length++));
			}
		}
	}

	private int mouseOnLetterID() {
		for (int i = 0; i < length; i++) {
			int blockX = Coords.x(lootblocklist.get(i).getX());
			int blockY = Coords.y(lootblocklist.get(i).getY());
			if (mouseX > blockX && mouseX < blockX + Coords.x(1) + 1 && mouseY > blockY && mouseY < blockY + Coords.y(1) + 2) {
				return i;
			}
		}
		return -1;
	}

	private boolean onLetters() {
		int firstX = Coords.x(lootblocklist.get(0).getX());
		int lastX = Coords.x(lootblocklist.get(length - 1).getX() + 1);
		int firstY = Coords.y(lootblocklist.get(0).getY());
		int lastY = Coords.y(lootblocklist.get(length - 1).getY() + 1);
		return mouseX > firstX && mouseX < lastX && mouseY > firstY && mouseY < lastY;
	}

	private void resetColors() {
		for (LootScreenBlock l : lootblocklist) {
			l.setLightColor();
		}
	}

	private void makeClickedBlocksBlack() {
		for (int i = 0; i < length; i++) {
			if (lootblocklist.get(i).getColor() == Color.gray) {
				lootblocklist.get(i).setBlackColor();
			}
		}
	}

	private int countDarkBlocks() {
		int count = 0;
		for (LootScreenBlock l : lootblocklist) {
			if (l.getColor() == Color.black || l.getColor() == Color.gray) {
				count++;
			}
		}
		return count;
	}

	public ArrayList<LootScreenBlock> getLootBlockList() {
		return lootblocklist;
	}
}
