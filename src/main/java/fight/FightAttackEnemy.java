package fight;

import enums.Attack;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import states.FightState;

public class FightAttackEnemy {
	private int x;
	private int y;
	private Attack attacke;
	private int key;
	private FightLog kampflog;
	private FightAttackBar kampfscreenattackebalken;
	private FightState fightState;
	private int gegnerzeit;
	private boolean gegnerhatzeitgewaehlt;
	private int timer;



	public FightAttackEnemy(int x, int y, Attack attacke, int key, FightLog kampflog, FightState fightState) {
		this.x = x;
		this.y = y;
		this.attacke = attacke;
		this.key = key;
		this.kampflog = kampflog;
		this.fightState = fightState;

		kampfscreenattackebalken = new FightAttackBar(x, y, attacke.getCooldown(), true);
	}

	public void update(GameContainer gc, int delta) {
		kampfscreenattackebalken.update(delta);

		if (!gegnerhatzeitgewaehlt) {
			gegnerzeit = Global.randomNumber((int) attacke.getCooldown() - 100, (int) attacke.getCooldown() + 800);
			gegnerhatzeitgewaehlt = true;
			timer = 0;
		}
		timer += delta;

		if (timer > gegnerzeit) {
			if (kampfscreenattackebalken.canMakeDamage()) {
				if (Global.randomNumber(0, 100) <= attacke.getProbability()) {
					if (kampfscreenattackebalken.getCritCounter() > 500 && kampfscreenattackebalken.getCritCounter() < 600) {
						fightState.getDamage(attacke.getDamage() * 2);
						kampflog.addAttack(key, false, Color.red);
					} else {
						fightState.getDamage(attacke.getDamage());
						kampflog.addAttack(key, false, Color.black);
					}
					kampfscreenattackebalken.reset();
					gegnerhatzeitgewaehlt = false;
				} else {
					kampflog.addAttack(key, false, Global.getGrey(234));
					kampfscreenattackebalken.reset();
					gegnerhatzeitgewaehlt = false;
				}
			} else {
				kampfscreenattackebalken.reset();
				gegnerhatzeitgewaehlt = false;
			}
		}
	}

	public void render(Graphics g) {
		kampfscreenattackebalken.render(g);

		g.setColor(Color.black);
		Global.drawSpacedString(g, attacke.getName(), x + (18 - attacke.getName().length()), y + 1);

		g.setColor(Color.gray);
		Global.drawSpacedString(g, attacke.getDamage() + "", x + (18 - (attacke.getDamage() + "").length()), y + 2);
		Global.drawSpacedString(g, attacke.getProbability() + "%", x, y + 2);
	}
}
