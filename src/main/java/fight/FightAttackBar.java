package fight;

import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class FightAttackBar {
	private int x;
	private int y;
	private int barSize = 18;
	private Color color;
	private boolean isBarMirrored;

	private double currentBar = 0;
	private double maxBar;
	private boolean isBarFilled;

	private double critCounter;
	private int critStart = 500;
	private int critEnd = 600;

	FightAttackBar(int x, int y, double currentBar, boolean isBarMirrored) {
		this.x = x;
		this.y = y;
		this.maxBar = currentBar;
		this.isBarMirrored = isBarMirrored;
	}

	public void update(int delta) {
		if (currentBar < maxBar) {
			color = Global.getGrey(175);
			currentBar += delta;
			isBarFilled = false;
		} else {
			color = Color.gray;
			critCounter += delta;
			isBarFilled = true;
			if (critCounter > critStart && critCounter < critEnd) {
				color = Color.black;
			}
		}
	}

	public void render(Graphics g) {
		g.setColor(Global.getGrey(235));
		Global.drawSpacedString(g, Global.charBar(barSize, '*'), x, y);

		g.setColor(color);

		if (isBarMirrored) {
			Global.drawSpacedString(g, Global.charBar(currentBarLength(), '*'), x + (18 - currentBarLength()), y);
		} else {
			Global.drawSpacedString(g, Global.charBar(currentBarLength(), '*'), x, y);
		}
	}

	private int currentBarLength() {
		double percentage = currentBar / maxBar;
		double currentBarLength = percentage * barSize;
		return (int) currentBarLength;
	}

	void reset() {
		currentBar = 0;
		critCounter = 0;
	}

	double getCritCounter() {
		return critCounter;
	}

	boolean canMakeDamage() {
		return isBarFilled;
	}
}
