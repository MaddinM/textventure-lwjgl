package fight;

import enums.Enemy;
import enums.Weapon;

// TODO rename class
public class FightEnemy {
	private String name;
	private int maxleben;
	private int leben;
	private Weapon waffe1;
	private Weapon waffe2;

	public FightEnemy(Enemy enemy, int level) {
		name = enemy.getName();
		maxleben = enemy.getHealth();
		leben = maxleben;

		waffe1 = enemy.getWeapon1();
		if (enemy.getWeapon2() != null) {
			waffe2 = enemy.getWeapon2();
		}
	}

	Weapon getWaffe1() {
		return waffe1;
	}

	Weapon getWaffe2() {
		return waffe2;
	}

	public String getName() {
		return name;
	}

	public int getLeben() {
		return leben;
	}

	public int getMaxLeben() {
		return maxleben;
	}

	public void setLeben(int i) {
		leben += i;
	}
}
