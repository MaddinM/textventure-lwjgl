package fight;

import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import save.PlayerData;

public class FightBackground {
	private FightEnemy gegner;

	public FightBackground(FightEnemy gegner) {
		this.gegner = gegner;
	}

	public void render(Graphics g) {
		g.setColor(Global.getGrey(226));
		for (int i = 1; i < Coords.maxX() / 3 - 2; i++) {
			Global.drawSpacedString(g, "-", Coords.maxX() / 3 + i, 4);
			Global.drawSpacedString(g, "-", Coords.maxX() / 3 + i, 8);
			Global.drawSpacedString(g, "-", Coords.maxX() / 3 + i, 12);
			Global.drawSpacedString(g, "-", Coords.maxX() / 3 + i, 16);
			Global.drawSpacedString(g, "-", Coords.maxX() / 3 * 2 + i + 1, 4);
			Global.drawSpacedString(g, "-", Coords.maxX() / 3 * 2 + i + 1, 8);
			Global.drawSpacedString(g, "-", Coords.maxX() / 3 * 2 + i + 1, 12);
			Global.drawSpacedString(g, "-", Coords.maxX() / 3 * 2 + i + 1, 16);
		}

		g.setColor(Color.black);
		Global.drawSpacedString(g, PlayerData.NAME, Coords.maxX() / 3 + 1, 17);
		Global.drawSpacedString(g, gegner.getName(), Coords.maxX() / 3 * 2 + 2, 17);
	}
}
