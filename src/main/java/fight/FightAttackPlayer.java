package fight;

import enums.Attack;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import states.FightState;

public class FightAttackPlayer {
	private int x;
	private int y;
	private Attack attacke;
	private int key;
	private FightLog kampflog;
	private FightAttackBar kampfscreenattackebalken;
	private FightState fightState;

	/**
	 * Konstruktor gibt Werte weiter und erstellt den Bar, der anzeigt wie
	 * aufgeladen die Attacke ist.
	 * 
	 * @param x        X-Koordinate.
	 * @param y        Y-Koordinate.
	 * @param attacke  Welche Attacke angezeigt wird.
	 * @param key    Mit welcher Taste die Attacke benutzt wird.
	 * @param kampflog FightLog-Klasse wird weitergegeben damit sie von hier aus
	 *                 benutzt werden kann.
	 * @param fightState    FightState-Klasse wird weitergegeben damit Werte von hier aus
	 *                 veraendert werden koennen.
	 */
	public FightAttackPlayer(int x, int y, Attack attacke, int key, FightLog kampflog, FightState fightState) {
		this.x = x;
		this.y = y;
		this.attacke = attacke;
		this.key = key;
		this.kampflog = kampflog;
		this.fightState = fightState;

		kampfscreenattackebalken = new FightAttackBar(x, y, attacke.getCooldown(), false);
	}

	public void update(GameContainer gc, int delta) {
		kampfscreenattackebalken.update(delta);

		if (gc.getInput().isKeyPressed(key)) {
			if (kampfscreenattackebalken.canMakeDamage()) {
				if (Global.randomNumber(0, 100) <= attacke.getProbability()) {
					if (kampfscreenattackebalken.getCritCounter() > 500 && kampfscreenattackebalken.getCritCounter() < 600) {
						fightState.makeDamage(attacke.getDamage() * 2);
						kampflog.addAttack(key, true, Color.red);
					} else {
						fightState.makeDamage(attacke.getDamage());
						kampflog.addAttack(key, true, Color.black);
					}
					kampfscreenattackebalken.reset();
				} else {
					kampflog.addAttack(key, true, Global.getGrey(234));
					kampfscreenattackebalken.reset();
				}
			} else {
				kampfscreenattackebalken.reset();
			}
		}
	}

	public void render(Graphics g) {
		kampfscreenattackebalken.render(g);

		g.setColor(Color.black);
		Global.drawSpacedString(g, attacke.getName(), x, y + 1);

		g.setColor(Color.gray);
		Global.drawSpacedString(g, attacke.getDamage() + "", x, y + 2);
		Global.drawSpacedString(g, attacke.getProbability() + "%", x + (18 - (attacke.getProbability() + "%").length()), y + 2);
	}
}
