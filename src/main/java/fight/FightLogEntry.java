package fight;

import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class FightLogEntry {
	private int y;

	private String key;
	private boolean eigeneattacke;
	private Color color;

	FightLogEntry(int y, int key, boolean eigeneattacke, Color color) {
		this.y = y;

		switch (key) {
		case Input.KEY_Q:
			this.key = "Q";
			break;
		case Input.KEY_W:
			this.key = "W";
			break;
		case Input.KEY_E:
			this.key = "E";
			break;
		case Input.KEY_R:
			this.key = "R";
			break;
		default:
			this.key = "";
		}

		this.eigeneattacke = eigeneattacke;
		this.color = color;
	}

	public void render(Graphics g) {
		if (eigeneattacke) {
			Global.drawSpacedString(g, (key + " "), Coords.maxX() / 3 * 2 - 1, y, color);
		} else {
			Global.drawSpacedString(g, (" " + key), Coords.maxX() / 3 * 2 - 1, y, color);
		}
	}

	public int getY() {
		return y;
	}

	void yErhoehen() {
		y++;
	}
}
