package fight;

import main.Coords;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import java.util.ArrayList;

public class FightLog {
	private ArrayList<FightLogEntry> log;

	FightLog() {
		log = new ArrayList<>();
	}

	public void render(Graphics g) {
		for (FightLogEntry kle : log) {
			if (kle.getY() < Coords.maxY() - 1) {
				kle.render(g);
			}
		}
	}

	void addAttack(int key, boolean eigeneattacke, Color color) {
		for (FightLogEntry kle : log) {
			kle.yErhoehen();
		}
		log.add(new FightLogEntry(1, key, eigeneattacke, color));
	}
}