package fight;

import main.Coords;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import save.PlayerData;
import states.FightState;

// TODO rename class for clarity
public class FightAttacks {
	private FightAttackPlayer playerWeapon1Attack1;
	private FightAttackPlayer playerWeapon1Attack2;
	private FightAttackPlayer playerWeapon2Attack1;
	private FightAttackPlayer playerWeapon2Attack2;

	private FightAttackEnemy enemyWeapon1Attack1;
	private FightAttackEnemy enemyWeapon1Attack2;
	private FightAttackEnemy enemyWeapon2Attack1;
	private FightAttackEnemy enemyWeapon2Attack2;

	private FightLog fightLog;

	public FightAttacks(FightEnemy gegner, FightState fightState) {
		fightLog = new FightLog();

		playerWeapon1Attack1 = new FightAttackPlayer(Coords.maxX() / 3 + 1, 1, PlayerData.WAFFE1.getAttack1(), Input.KEY_Q, fightLog, fightState);
		if (PlayerData.WAFFE1.getAttack2() != null) {
			playerWeapon1Attack2 = new FightAttackPlayer(Coords.maxX() / 3 + 1, 5, PlayerData.WAFFE1.getAttack2(), Input.KEY_W, fightLog, fightState);
		}

		if (PlayerData.WAFFE2 != null) {
			playerWeapon2Attack1 = new FightAttackPlayer(Coords.maxX() / 3 + 1, 9, PlayerData.WAFFE2.getAttack1(), Input.KEY_E, fightLog, fightState);
			if (PlayerData.WAFFE2.getAttack2() != null) {
				playerWeapon2Attack2 = new FightAttackPlayer(Coords.maxX() / 3 + 1, 13, PlayerData.WAFFE2.getAttack2(), Input.KEY_R, fightLog, fightState);
			}
		}

		enemyWeapon1Attack1 = new FightAttackEnemy(Coords.maxX() / 3 * 2 + 2, 1, gegner.getWaffe1().getAttack1(), Input.KEY_Q, fightLog, fightState);
		if (gegner.getWaffe1().getAttack2() != null) {
			enemyWeapon1Attack2 = new FightAttackEnemy(Coords.maxX() / 3 * 2 + 2, 5, gegner.getWaffe1().getAttack2(), Input.KEY_W, fightLog, fightState);
		}

		if (gegner.getWaffe2() != null) {
			enemyWeapon2Attack1 = new FightAttackEnemy(Coords.maxX() / 3 * 2 + 2, 9, gegner.getWaffe2().getAttack1(), Input.KEY_E, fightLog, fightState);
			if (gegner.getWaffe2().getAttack2() != null) {
				enemyWeapon2Attack2 = new FightAttackEnemy(Coords.maxX() / 3 * 2 + 2, 13, gegner.getWaffe2().getAttack2(), Input.KEY_R, fightLog, fightState);
			}
		}
	}

	public void render(Graphics g) {
		playerWeapon1Attack1.render(g);
		if (playerWeapon1Attack2 != null) {
			playerWeapon1Attack2.render(g);
		}

		if (PlayerData.WAFFE2 != null) {
			playerWeapon2Attack1.render(g);
			if (playerWeapon2Attack2 != null) {
				playerWeapon2Attack2.render(g);
			}
		}

		enemyWeapon1Attack1.render(g);
		if (enemyWeapon1Attack2 != null) {
			enemyWeapon1Attack2.render(g);
		}

		if (enemyWeapon2Attack1 != null) {
			enemyWeapon2Attack1.render(g);
			if (enemyWeapon2Attack2 != null) {
				enemyWeapon2Attack2.render(g);
			}
		}

		fightLog.render(g);
	}

	public void update(GameContainer gc, int delta) {
		playerWeapon1Attack1.update(gc, delta);
		if (playerWeapon1Attack2 != null) {
			playerWeapon1Attack2.update(gc, delta);
		}

		if (PlayerData.WAFFE2 != null) {
			playerWeapon2Attack1.update(gc, delta);
			if (playerWeapon2Attack2 != null) {
				playerWeapon2Attack2.update(gc, delta);
			}
		}

		enemyWeapon1Attack1.update(gc, delta);
		if (enemyWeapon1Attack2 != null) {
			enemyWeapon1Attack2.update(gc, delta);
		}

		if (enemyWeapon2Attack1 != null) {
			enemyWeapon2Attack1.update(gc, delta);
			if (enemyWeapon2Attack2 != null) {
				enemyWeapon2Attack2.update(gc, delta);
			}
		}
	}
}
