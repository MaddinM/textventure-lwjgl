package enums;

import main.Constants;

// TODO connect to enum Item somehow

public enum Weapon {
	SMALL_DAGGER(
			Constants.STRINGS.getString("small_dagger"),
			Attack.DAGGER_STAB,
			Attack.THROW),
	TWO_HANDED_SWORD(
			Constants.STRINGS.getString("two_handed_sword"),
			Attack.STRONG_BLOW,
			Attack.RUN_UP),
	SPIDER_BITE(
			Constants.STRINGS.getString("spider_bite"),
			Attack.BITE);

	private String name;
	private Attack attack1;
	private Attack attack2;

	Weapon(String name, Attack attack1, Attack attack2) {
		this.name = name;
		this.attack1 = attack1;
		this.attack2 = attack2;
	}

	Weapon(String name, Attack attack1) {
		this.name = name;
		this.attack1 = attack1;
	}

	public String getName() {
		return name;
	}

	public Attack getAttack1() {
		return attack1;
	}

	public Attack getAttack2() {
		return attack2;
	}
}
