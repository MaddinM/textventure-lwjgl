package enums;

import main.Constants;
import org.newdawn.slick.Color;

public enum Rareness {
	ORDINARY(
			Constants.STRINGS.getString("ordinary"),
			Color.white),
	UNUSUAL(
			Constants.STRINGS.getString("unusual"),
			Color.green),
	RARE(
			Constants.STRINGS.getString("rare"),
			Color.blue),
	EPIC(
			Constants.STRINGS.getString("epic"),
			Color.pink),
	LEGENDARY(
			Constants.STRINGS.getString("legendary"),
			Color.yellow);

	private String name;
	private Color color;

	Rareness(String name, Color color) {
		this.name = name;
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public Color getColor() {
		return color;
	}
}
