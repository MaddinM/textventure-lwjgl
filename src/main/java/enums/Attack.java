package enums;

import main.Constants;

public enum Attack {
	DAGGER_STAB(
			Constants.STRINGS.getString("dagger_stab"),
			2,
			85,
			1500),
	THROW(
			Constants.STRINGS.getString("throw"),
			8,
			45,
			3500),
	STRONG_BLOW(
			Constants.STRINGS.getString("strong_blow"),
			22,
			60,
			10000),
	RUN_UP(
			Constants.STRINGS.getString("run_up"),
			17,
			75,
			6500),
	BITE(
			Constants.STRINGS.getString("bite"),
			2,
			90,
			3000);

	private String name;
	private int damage;
	private int probability;
	private int cooldown;

	Attack(String name, int damage, int probability, int cooldown) {
		this.name = name;
		this.damage = damage;
		this.probability = probability;
		this.cooldown = cooldown;
	}

	public String getName() {
		return name;
	}

	public int getDamage() {
		return damage;
	}

	public double getCooldown() {
		return cooldown;
	}

	public int getProbability() {
		return probability;
	}
}
