package enums;

import main.Constants;

public enum CauseOfDeath {
	DIED_OF_THIRST(
			Constants.STRINGS.getString("death_message_thirst")
	),
	KILLED_IN_FIGHT(
			Constants.STRINGS.getString("death_message_killed_in_fight")
	);

	private String deathMessage;

	CauseOfDeath(String deathMessage) {
		this.deathMessage = deathMessage;
	}

	public String getDeathMessage() {
		return deathMessage;
	}
}
