package enums;

public enum Equipment {
	HELMET,
	BREASTPLATE,
	PANTS,
	BOOTS,
	BAG,
	BOTTLE,
	WEAPON
}
