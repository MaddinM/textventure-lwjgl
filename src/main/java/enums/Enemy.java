package enums;

import main.Constants;

import java.util.Random;

public enum Enemy {
	SPIDER(
			Constants.STRINGS.getString("spider"),
			50,
			Weapon.SPIDER_BITE),
	SOLDIER(
			Constants.STRINGS.getString("soldier"),
			150,
			Weapon.SMALL_DAGGER,
			Weapon.TWO_HANDED_SWORD);

	private String name;
	private int health;
	private Weapon weapon1;
	private Weapon weapon2;

	Enemy(String name, int health, Weapon weapon1, Weapon weapon2) {
		this.name = name;
		this.health = health;
		this.weapon1 = weapon1;
		this.weapon2 = weapon2;
	}

	Enemy(String name, int health, Weapon weapon1) {
		this.name = name;
		this.health = health;
		this.weapon1 = weapon1;
	}

	public String getName() {
		return name;
	}

	public int getHealth() {
		return health;
	}

	public Weapon getWeapon1() {
		return weapon1;
	}

	public Weapon getWeapon2() {
		return weapon2;
	}

	public static Enemy getRandomEnemy() {
		return Enemy.values()[new Random().nextInt(Enemy.values().length)];
	}
}
