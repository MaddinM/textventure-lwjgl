package enums;

public enum State {
	// Textventure Animation when opening the game
	OPENSCREEN(0),
	// The state after the animation, where the res folder is amountLoadedFiles
	LOADINGSCREEN(1),
	MENU(2),
	LOADSAVES(3),
	OPTIONS(4),
	CREDITS(5),
	INGAME(6),
	INVENTORY(7),
	DEATHSCREEN(8),
	LOOTSCREEN(9),
	FIGHT(10),
	HOME(11),
	CREATECHARACTER(12),
	SAVE(13);

	private int id;

	State(int id) {
		this.id = id;
	}

	public int id() {
		return id;
	}
}