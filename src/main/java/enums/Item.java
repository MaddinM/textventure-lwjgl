package enums;

import main.Constants;

public enum Item {
	TORCH(
			Constants.STRINGS.getString("torch"),
			2,
			Rareness.ORDINARY);

	private String name;
	private int anzahl;
	private Rareness rareness;

	Item(String name, int anzahl, Rareness rareness) {
		this.name = name;
		this.anzahl = anzahl;
		this.rareness = rareness;
	}

	public void setAnzahl(int menge) {
		anzahl += menge;
	}

	public String getName() {
		return name;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public Rareness getRareness() {
		return rareness;
	}
}
