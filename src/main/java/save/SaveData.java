package save;

import enums.Item;
import world.WorldBlock;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class SaveData {

	public SaveData(String playername) {
		// if necessary, creates dir
		new File("profiles").mkdir();
		new File("profiles\\" + playername).mkdir();

		try {
			// save world
			PrintWriter printWriteWorldData = new PrintWriter("profiles\\" + playername + "\\" + playername + ".txvworld");
			for (WorldBlock wb : WorldData.get()) {
				printWriteWorldData.println(wb.getName() + " " + wb.getX() + " " + wb.getY());
			}
			printWriteWorldData.close();

			// save playerdata
			PrintWriter printWriterPlayerData = new PrintWriter("profiles\\" + playername + "\\" + playername + ".txvplayer");
			printWriterPlayerData.println(PlayerData.MAXHEALTH);
			printWriterPlayerData.println(PlayerData.CURRENTHEALTH);
			printWriterPlayerData.println(PlayerData.MAXWATER);
			printWriterPlayerData.println(PlayerData.CURRENTWATER);
			printWriterPlayerData.println(PlayerData.MAXINVENTORYSPACE);
			printWriterPlayerData.println(PlayerData.CURRENTINVENTORYUSED);
			printWriterPlayerData.println(PlayerData.LEVEL);
			printWriterPlayerData.println(PlayerData.WAFFE1);
			printWriterPlayerData.println(PlayerData.WAFFE2);
			printWriterPlayerData.close();

			// save options
			PrintWriter printWriterOptionsData = new PrintWriter("profiles\\" + playername + "\\" + playername + ".txvoptions");
			printWriterOptionsData.println("mim"); // TODO optionen
			printWriterOptionsData.close();

			// save inventory
			PrintWriter printWriterInventoryData = new PrintWriter("profiles\\" + playername + "\\" + playername + ".txvinventory");
			for (Item item : InventoryData.get()) {
				printWriterInventoryData.println(item);
			}
			printWriterInventoryData.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
