package save;

import enums.State;
import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.StateBasedGame;

public class SaveButton {
	private String playername;
	private String label;
	private int x;
	private int y;

	private boolean hover = false;

	public SaveButton(String playername, String label, int x, int y) {
		this.playername = playername;
		this.label = label;
		this.x = x;
		this.y = y;
	}

	public void render(Graphics g) {
		if (hover) {
			g.setColor(Color.black);
		} else {
			g.setColor(Color.lightGray);
		}

		Global.drawSpacedString(g, label, x, y);
	}

	public void update(GameContainer gc, StateBasedGame sbg) {
		Input input = gc.getInput();
		int mouseX = input.getMouseX();
		int mouseY = input.getMouseY();

		if ((mouseX > Coords.x(x) && mouseX < Coords.x(x + label.length())) && (mouseY > Coords.y(y) && mouseY < Coords.y(y + 1))) {
			hover = true;
		} else {
			hover = false;
		}

		if (hover && input.isMouseButtonDown(0)) {
			if (label.equals("Ja")) {
				new SaveData(playername);
			}
			sbg.enterState(State.HOME.id(), Global.fadeOut(), Global.fadeIn());
		}
	}
}
