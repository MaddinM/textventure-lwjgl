package save;

import enums.Weapon;

public class PlayerData {
	public static String NAME;

	public static int MAXHEALTH;
	public static int CURRENTHEALTH;

	public static int MAXWATER;
	public static int CURRENTWATER;

	public static int MAXINVENTORYSPACE;
	public static int CURRENTINVENTORYUSED;

	public static int LEVEL;
	public static Weapon WAFFE1;
	public static Weapon WAFFE2;

	public static void defaultPlayerData(String name) {
		NAME = name;
		MAXHEALTH = 100;
		CURRENTHEALTH = MAXHEALTH;
		MAXWATER = 5000;
		CURRENTWATER = MAXWATER;
		MAXINVENTORYSPACE = 10;
		CURRENTINVENTORYUSED = 0;
		LEVEL = 1;
		WAFFE1 = Weapon.SMALL_DAGGER;
		WAFFE2 = Weapon.TWO_HANDED_SWORD;
	}

	public static void setHealth(int newHealth) {

	}
}
