package save;

import enums.Item;
import java.util.ArrayList;

public class InventoryData {

	private static ArrayList<Item> inventarlist = new ArrayList<Item>();

	public static void createFirstItems() {
		add(Item.TORCH);
		add(Item.TORCH);
		add(Item.TORCH);
		add(Item.TORCH);
		add(Item.TORCH);
	}

	public static void add(Item item) {
		inventarlist.add(item);
	}

	public static ArrayList<Item> get() {
		return inventarlist;
	}

	public static Item get(int i) {
		return inventarlist.get(i);
	}

	public static int size() {
		return inventarlist.size();
	}
}
