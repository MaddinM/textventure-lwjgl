package save;

import main.Coords;
import main.Global;
import world.WorldBlock;
import world.WorldGenerator;

import java.util.ArrayList;

public class WorldData {
	private static ArrayList<WorldBlock> worldlist = new ArrayList<>();
	public static final int playerX = Coords.maxX() / 3 * 2;
	public static final int playerY = Coords.maxY() / 2;
	
	public static void defaultWorldData() {
		add(new WorldBlock(Global.randomLetter(WorldGenerator.letters), playerX, playerY - 2));

		add(new WorldBlock(Global.randomLetter(WorldGenerator.letters), playerX - 1, playerY - 1));
		add(new WorldBlock(Global.randomLetter(WorldGenerator.letters), playerX, playerY - 1));
		add(new WorldBlock(Global.randomLetter(WorldGenerator.letters), playerX + 1, playerY - 1));

		add(new WorldBlock(Global.randomLetter(WorldGenerator.letters), playerX - 2, playerY));
		add(new WorldBlock(Global.randomLetter(WorldGenerator.letters), playerX - 1, playerY));

		add(new WorldBlock("Z", playerX, playerY));

		add(new WorldBlock(Global.randomLetter(WorldGenerator.letters), playerX + 1, playerY));
		add(new WorldBlock(Global.randomLetter(WorldGenerator.letters), playerX + 2, playerY));

		add(new WorldBlock(Global.randomLetter(WorldGenerator.letters), playerX - 1, playerY + 1));
		add(new WorldBlock(Global.randomLetter(WorldGenerator.letters), playerX, playerY + 1));
		add(new WorldBlock(Global.randomLetter(WorldGenerator.letters), playerX + 1, playerY + 1));

		add(new WorldBlock(Global.randomLetter(WorldGenerator.letters), playerX, playerY + 2));
	}

	public static void add(WorldBlock wb) {
		worldlist.add(wb);
	}

	public static void clear() {
		worldlist.clear();
	}

	public static ArrayList<WorldBlock> get() {
		return worldlist;
	}

	public static void set(ArrayList<WorldBlock> newlist) {
		worldlist = newlist;
	}
}
