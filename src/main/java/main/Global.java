package main;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import java.nio.CharBuffer;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Global {
	private static int[] allkeys = { Input.KEY_0, Input.KEY_1, Input.KEY_2, Input.KEY_3, Input.KEY_4, Input.KEY_5, Input.KEY_6, Input.KEY_7,
			Input.KEY_8, Input.KEY_9, Input.KEY_A, Input.KEY_B, Input.KEY_C, Input.KEY_D, Input.KEY_E, Input.KEY_F, Input.KEY_G, Input.KEY_H,
			Input.KEY_I, Input.KEY_J, Input.KEY_K, Input.KEY_L, Input.KEY_M, Input.KEY_N, Input.KEY_O, Input.KEY_P, Input.KEY_Q, Input.KEY_R,
			Input.KEY_S, Input.KEY_T, Input.KEY_U, Input.KEY_V, Input.KEY_W, Input.KEY_X, Input.KEY_Y, Input.KEY_Z, Input.KEY_SPACE };

	private static int fadeTime = 300;

	public static void drawSpacedString(Graphics g, String s, int x, int y) {
		int lenght = 0;
		for (int i = 0; i < s.length(); i++) {
			g.drawString(s.substring(lenght, lenght + 1), Coords.x(x + i), Coords.y(y));
			lenght++;
		}
	}

	public static void drawSpacedString(Graphics g, String s, int x, int y, Color color) {
		int lenght = 0;
		g.setColor(color);
		for (int i = 0; i < s.length(); i++) {
			g.drawString(s.substring(lenght, lenght + 1), Coords.x(x + i), Coords.y(y));
			lenght++;
		}
	}

	public static void drawSpacedString(Graphics g, String s, int x, int y, int scroll, Color color) {
		int lenght = 0;
		g.setColor(color);
		for (int i = 0; i < s.length(); i++) {
			g.drawString(s.substring(lenght, lenght + 1), Coords.x(x + i), Coords.y(y) + scroll);
			lenght++;
		}
	}

	public static String randomLetter(String letters) {
		Random rnd = new Random();
		// TODO use intellij's fix and check lootscreen crash
		StringBuilder sb = new StringBuilder();
		return sb.append(letters.charAt(rnd.nextInt(letters.length()))).toString();
	}

	public static String randomLetter() {
		return ((char) ThreadLocalRandom.current().nextInt(33, 127)) + "";
	}

	public static int randomNumber(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}

	public static String charBar(int laenge, char zeichen) {
		return CharBuffer.allocate(laenge).toString().replace('\0', zeichen);
	}

	public static Color getGrey(int rbg) {
		return new Color(rbg, rbg, rbg);
	}

	public static String getKeyString(GameContainer gc) {
		String returnletter = "";
		Input input = gc.getInput();
		for (int key : allkeys) {
			if (input.isKeyPressed(key)) {
				if (input.isKeyDown(Input.KEY_LSHIFT)) {
					returnletter = Input.getKeyName(key);
				} else {
					returnletter = Input.getKeyName(key).toLowerCase();
				}

				if (returnletter.toLowerCase().equals("space")) {
					returnletter = " ";
				}
			}
		}
		return returnletter;
	}

	public static String[] getStringArray(String string) {
		return string.split(";");
	}

	public static FadeOutTransition fadeOut() {
		return new FadeOutTransition(Color.black, fadeTime);
	}

	public static FadeInTransition fadeIn() {
		return new FadeInTransition(Color.black, fadeTime);
	}
}
