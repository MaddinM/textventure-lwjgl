package main;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

import javax.swing.*;

public class Audio {

	public int amountLoadedFiles;
	/**
	 * Gibt an ob alle Dateien bereits geladen wurden. Bei true wird der State
	 * gewechselt.
	 */
	public boolean finishedLoading = false;

	/** Musik für Bosskämpfe */
	public static Music boss;

	/** Musik während die CreditsState laufen */
	public static Music credits;

	/** Alternative Musik für die CreditsState */
	public static Music credits2;

	/** Musik, wenn man sich auf der Karte bewegt */
	public static Music draussen;

	/** Musik, wenn man in ein Gebäude geht(?) */
	public static Music erkunden;

	/** Musik, wenn man nach einem FightState im Looten-State befindet */
	public static Music looten;

	/** Musik, die während des (noch nicht vorhandenen) Intros spielt */
	public static Music intro;

	/** Musik für Kämpfe */
	public static Music kampf;

	/** Alternative Musik für Kämpfe */
	public static Music kampf2;

	/** Musik, wenn man sich in einem MenuState befindet (InventoryState etc) */
	public static Music menu;

	/** Musik, wenn man sich HomeState befindet */
	public static Music zuhause;
	
	public static Sound down;

	/**
	 * Hier wird geprüft, ob alle Dateien da sind und werden dann geladen. Das
	 * passiert, indem die initialisiert werden.
	 * 
	 * @param g Graphics
	 */
	// TODO gucken warum das im render sein muss und nicht im update
	public void render(Graphics g) {
		if (!finishedLoading) {
			try {
				switch (amountLoadedFiles) {
				case 0:
					boss = new Music(Constants.RES_FOLDER + "music/boss.ogg");
					break;
				case 1:
					credits = new Music(Constants.RES_FOLDER + "music/credits.ogg");
					break;
				case 2:
					credits2 = new Music(Constants.RES_FOLDER + "music/credits2.ogg");
					break;
				case 3:
					draussen = new Music(Constants.RES_FOLDER + "music/draussen.ogg");
					break;
				case 4:
					erkunden = new Music(Constants.RES_FOLDER + "music/erkunden.ogg");
					break;
				case 5:
					looten = new Music(Constants.RES_FOLDER + "music/looten.ogg");
					break;
				case 6:
					intro = new Music(Constants.RES_FOLDER + "music/intro.ogg");
					break;
				case 7:
					kampf = new Music(Constants.RES_FOLDER + "music/kampf.ogg");
					break;
				case 8:
					kampf2 = new Music(Constants.RES_FOLDER + "music/kampf2.ogg");
					break;
				case 9:
					menu = new Music(Constants.RES_FOLDER + "music/menu.ogg");
					break;
				case 10:
					zuhause = new Music(Constants.RES_FOLDER + "music/zuhause.ogg");
					break;
				case 11:
					down = new Sound(Constants.RES_FOLDER + "sound/down.ogg");
				}
			} catch (SlickException e) {
				JOptionPane.showMessageDialog(null, e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			}
			amountLoadedFiles++;
			if (amountLoadedFiles == Constants.AMOUNT_FILES_TO_LOAD) {
				finishedLoading = true;
			}
		}
	}
}