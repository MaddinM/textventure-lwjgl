package main;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Border {
	private StringBuilder horizontalLine = new StringBuilder("+" + Global.charBar(Coords.maxX() - 2, '-') + "+");
	private int[] x;

	public Border(int... x) {
		for (int value : x) {
			horizontalLine.setCharAt(value, '+');
		}

		this.x = x;
	}

	public void render(Graphics g) {
		g.setColor(Color.gray);

		Global.drawSpacedString(g, horizontalLine.toString(), 0, 0);
		Global.drawSpacedString(g, horizontalLine.toString(), 0, Coords.maxY() - 1);

		for (int i = 0; i < Coords.maxY() - 2; i++) {
			g.drawString("|", Coords.x(0), Coords.y(i + 1));
			g.drawString("|", Coords.x(95), Coords.y(i + 1));
		}

		for (int j : x) {
			for (int i = 0; i < Coords.maxY() - 2; i++) {
				g.drawString("|", Coords.x(j), Coords.y(i + 1));
			}
		}
	}
}