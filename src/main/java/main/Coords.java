package main;

public class Coords {
	public static int x(int x) {
		//return x * 20;
		return x * Constants.WINDOW_WIDTH / maxX();
	}

	public static int y(int y) {
		//return y * 30;
		return y * Constants.WINDOW_HEIGHT / maxY();
	}
	
	public static int maxX() {
		return 96;
	}
	
	public static int maxY() {
		return 32;
	}
}