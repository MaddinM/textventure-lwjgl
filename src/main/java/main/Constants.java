package main;

import org.newdawn.slick.TrueTypeFont;

import java.io.File;
import java.util.ResourceBundle;

public class Constants {

    public static final String VERSION = "0.11.0";
    public static final String RES_FOLDER = "resources/";

    public static int WINDOW_WIDTH = 1600;//GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().width;
    public static int WINDOW_HEIGHT = 900;//WINDOW_WIDTH / 16 * 9;
    public static boolean FULLSCREEN = false;
    static final int MAXFPS = 60;

    public static float VOLUME = 0.01f; // Standard: 0.1f, if annoying 0.01f
    public static float VOLUME_SOUND = 0.1f;

    public static TrueTypeFont FONT;

    // TODO make this safe
    public static final int AMOUNT_FILES_TO_LOAD = new File(RES_FOLDER + "music/").list().length - 1 + new File(RES_FOLDER + "sound/").list().length - 4;
    public static ResourceBundle STRINGS = ResourceBundle.getBundle("Strings");

    // main.Textventure-Logo represented as one string
    public static final String TEXTVENTURE_LOGO = " _____         _                   _                  |_   _|____  _| |___   ____"
            + "_ _ __ | |_ _   _ _ __ ___   | |/ _ \\ \\/ / __\\ \\ / / _ \\ '_ \\| __| | | | '__/ _ \\  | |  __/>  <| "
            + "|_ \\ V /  __/ | | | |_| |_| | | |  __/  |_|\\___/_/\\_\\\\__| \\_/ \\___|_| |_|\\__|\\__,_|_|  \\___|";

    // main.Textventure-Logo background represented as one string
    public static final String TEXTVENTURE_LOGO_BG = "#####         #                   #                   #  ### #  # ### #   # ### ####  ### # "
            + "  # #### ###  # ####  ##  #    # # #### #   # #   #   # #   ####  #  ### #  #  ##   #   ### #   #  ##  #### #    ###";
}
