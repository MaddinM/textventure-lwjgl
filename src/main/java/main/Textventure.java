package main;

import enums.State;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import states.*;

import javax.swing.*;

/**
 * Textventure - A text based adventure game
 * 
 * @author Maddin
 * @since 23.12.2014
 */
public class Textventure extends StateBasedGame {
	public static AppGameContainer appgc;

	public Textventure(String gamename) {
		super(gamename);
		//Mouse.setGrabbed(true);
	}

	public void initStatesList(GameContainer gc) {
		this.addState(new OpenScreenState(State.OPENSCREEN));
		this.addState(new LoadingScreenState(State.LOADINGSCREEN));
		this.addState(new MenuState(State.MENU));
		this.addState(new LoadSavesState(State.LOADSAVES));
		this.addState(new OptionsState(State.OPTIONS));
		this.addState(new CreditsState(State.CREDITS));
		this.addState(new IngameState(State.INGAME));
		this.addState(new InventoryState(State.INVENTORY));
		this.addState(new DeathScreenState(State.DEATHSCREEN));
		this.addState(new LootScreenState(State.LOOTSCREEN));
		this.addState(new FightState(State.FIGHT));
		this.addState(new HomeState(State.HOME));
		this.addState(new CreateCharacterState(State.CREATECHARACTER));
		this.addState(new SaveState(State.SAVE));
	}

	public static void main(String... args) {
		try {
			appgc = new AppGameContainer(new Textventure("Textventure " + Constants.VERSION));
			appgc.setDisplayMode(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT, Constants.FULLSCREEN);
			appgc.setTargetFrameRate(Constants.MAXFPS);
			appgc.setShowFPS(false);
			appgc.setVSync(true);
			appgc.setIcon(Constants.RES_FOLDER + "img/icon.png");
			appgc.start();
		} catch (SlickException e) {
			JOptionPane.showMessageDialog(null, e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}