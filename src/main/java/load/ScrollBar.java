package load;

import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class ScrollBar {
	private double amountOfItems;
	private double scroll;
	private int scrollBarSize;
	private int maxOnScreen;
	private int x;
	private int y;
	private Color color;

	public ScrollBar(int x, int y, int scrollBarSize, int maxOnScreen, double amountOfItems, Color color) {
		this.amountOfItems = amountOfItems;
		this.scrollBarSize = scrollBarSize;
		this.maxOnScreen = maxOnScreen;
		this.x = x;
		this.y = y;
		this.color = color;
	}

	public void render(Graphics g) {
		g.setColor(color);
		if (amountOfItems > maxOnScreen) {
			for (int i = 0; i < scrollBarSize(); i++) {
				Global.drawSpacedString(g, "#", x, y + i + (int) scroll);
			}
		}
	}

	private double scrollBarSize() {
		return (maxOnScreen / amountOfItems) * scrollBarSize;
	}

	private double scrollBarMovePerScroll() {
		return (scrollBarSize - scrollBarSize()) / (amountOfItems - maxOnScreen);
	}

	public void moveScrollBarUp() {
		scroll += scrollBarMovePerScroll();
	}

	public void moveScrollBarDown() {
		scroll -= scrollBarMovePerScroll();
	}
}
