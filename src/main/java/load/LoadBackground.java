package load;

import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class LoadBackground {
	public void render(Graphics g) {
		g.setColor(Color.black);
		Global.drawSpacedString(g, "Name", Coords.maxX() / 3 + 2, 2);

		g.setColor(Global.getGrey(235));
		for (int i = 0; i < 7; i++) {
			Global.drawSpacedString(
					g,
					Global.charBar(43, '-'),
					Coords.maxX() / 3 + 1,
					3 + 4 * i);
		}

		for (int i = 0; i < Coords.maxY() - 4; i++) {
			Global.drawSpacedString(
					g,
					"#",
					Coords.maxX() - 2,
					3 + i);
		}
	}
}
