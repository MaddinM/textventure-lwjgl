package load;

import main.Coords;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

import java.io.File;

public class ListSaveFiles {
	private File folder = new File("profiles");
	private File[] allSaves = folder.listFiles();
	private LoadButton[] buttons = new LoadButton[allSaves.length];
	private ScrollBar scrollbar;
	private int scroll;
	private int mousewheel;

	public ListSaveFiles() {
		scrollbar = new ScrollBar(
				Coords.maxX() - 2,
				3,
				Coords.maxY() - 1,
				7,
				allSaves.length,
				Color.gray);

		for (int i = 0; i < allSaves.length; i++) {
			buttons[i] = new LoadButton(allSaves[i].getName(), Coords.maxX() / 3 + 2, 5 + i * 4 + scroll);
		}
	}

	public void render(Graphics g) {
		scrollbar.render(g);

		for (LoadButton loadbutton : buttons) {
			loadbutton.render(g);
		}
	}

	public void update(GameContainer gc, StateBasedGame sbg) {
		for (LoadButton loadbutton : buttons) {
			loadbutton.update(gc, sbg, scroll);
		}

		mousewheel = Mouse.getDWheel();
		scrollUpOrDown(mousewheel);
	}

	private void scrollUpOrDown(int mousewheel) {
		if (allSaves.length > 7) {
			if (mousewheel > 0 && scroll < 0) {
				scroll += 4;
				scrollbar.moveScrollBarDown();
			}
			if (mousewheel < 0 && scroll > (allSaves.length - 7) * -3) {
				scroll -= 4;
				scrollbar.moveScrollBarUp();
			}
		}
	}
}