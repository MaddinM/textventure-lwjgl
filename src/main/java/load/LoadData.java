package load;

import enums.Item;
import enums.Weapon;
import save.InventoryData;
import save.PlayerData;
import save.WorldData;
import world.WorldBlock;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class LoadData {
	public LoadData(String playername) {
		WorldData.clear();
		PlayerData.NAME = playername;

		try {
			BufferedReader brworld = new BufferedReader(new FileReader("profiles\\" + playername + "\\" + playername + ".txvworld"));
			String lineworld;
			WorldData.clear();
			while ((lineworld = brworld.readLine()) != null) {
				String[] lineArray = lineworld.split(" ");
				WorldData.add(new WorldBlock(lineArray[0], Integer.parseInt(lineArray[1]), Integer.parseInt(lineArray[2])));
			}
			brworld.close();

			BufferedReader brplayer = new BufferedReader(new FileReader("profiles\\" + playername + "\\" + playername + ".txvplayer"));
			String lineplayer;
			String[] playerdata = new String[9];
			int i = 0;
			while ((lineplayer = brplayer.readLine()) != null) {
				playerdata[i++] = lineplayer;
			}
			PlayerData.MAXHEALTH = Integer.parseInt(playerdata[0]);
			PlayerData.CURRENTHEALTH = Integer.parseInt(playerdata[1]);
			PlayerData.MAXWATER = Integer.parseInt(playerdata[2]);
			PlayerData.CURRENTWATER = Integer.parseInt(playerdata[3]);
			PlayerData.MAXINVENTORYSPACE = Integer.parseInt(playerdata[4]);
			PlayerData.CURRENTINVENTORYUSED = Integer.parseInt(playerdata[5]);
			PlayerData.LEVEL = Integer.parseInt(playerdata[6]);
			PlayerData.WAFFE1 = Weapon.valueOf(playerdata[7]);
			PlayerData.WAFFE2 = Weapon.valueOf(playerdata[8]);
			brplayer.close();

			// TODO TXVOPTIONS laden

			BufferedReader brinventory = new BufferedReader(new FileReader("profiles\\" + playername + "\\" + playername + ".txvinventory"));
			String lineinventory;
			while ((lineinventory = brinventory.readLine()) != null) {
				InventoryData.add(Item.valueOf(lineinventory));
			}
			brinventory.close();

		} catch (NumberFormatException | IOException e) {
			JOptionPane.showMessageDialog(null, e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public static boolean exists(String playername) {
		return new File("profiles\\" + playername + "\\" + playername + ".txvworld").isFile()
				&& new File("profiles\\" + playername + "\\" + playername + ".txvplayer").isFile()
				&& new File("profiles\\" + playername + "\\" + playername + ".txvoptions").isFile()
				&& new File("profiles\\" + playername + "\\" + playername + ".txvinventory").isFile();
	}
}
