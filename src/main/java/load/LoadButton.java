package load;

import enums.State;
import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.StateBasedGame;

import javax.swing.*;

public class LoadButton {
	private String playername;
	private int x;
	private int y;
	private int scroll;

	private boolean hover = false;

	public LoadButton(String playername, int x, int y) {
		this.playername = playername;
		this.x = x;
		this.y = y;
	}

	public void render(Graphics g) {
		if (hover) {
			g.setColor(Color.black);
		} else {
			g.setColor(Color.lightGray);
		}

		if (visible()) {
			Global.drawSpacedString(g, playername, x, y + scroll);
		}
	}

	public void update(GameContainer gc, StateBasedGame sbg, int scroll) {
		this.scroll = scroll;

		Input input = gc.getInput();
		int mouseX = input.getMouseX();
		int mouseY = input.getMouseY();

		if ((mouseX > Coords.x(x) && mouseX < Coords.x(x + playername.length()))
				&& (mouseY > Coords.y(y + scroll) && mouseY < Coords.y(y + 1 + scroll)) && visible()) {
			hover = true;
		} else {
			hover = false;
		}

		if (hover && input.isMouseButtonDown(0)) {
			if (LoadData.exists(playername)) {
				new LoadData(playername);
				sbg.enterState(State.HOME.id(), Global.fadeOut(), Global.fadeIn());
			} else {
				JOptionPane.showMessageDialog(null, "Savefile(s) missing or damaged!", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private boolean visible() {
		return y + scroll >= 3 && y + scroll <= Coords.maxY();
	}
}
