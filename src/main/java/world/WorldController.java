package world;

import enums.State;
import ingame.Bar;
import main.Global;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.StateBasedGame;
import save.PlayerData;

public class WorldController {
	private Bar wasserbalken;
	private World world;
	private int randomenemy;
	private int enemychance = 5; // 100/n %
	private boolean didfirstmove = false;

	public WorldController(Bar wasserbalken, World world) {
		this.wasserbalken = wasserbalken;
		this.world = world;
	}

	public void update(GameContainer gc, StateBasedGame sbg) {
		Input input = gc.getInput();
		// Mit WASD bewegen
		if (wasserbalken.getCurrent() > 0) {
			if (input.isKeyPressed(Input.KEY_W) || input.isKeyPressed(Input.KEY_UP)) {
				world.up();
				doEveryMove();
			} else if (input.isKeyPressed(Input.KEY_S) || input.isKeyPressed(Input.KEY_DOWN)) {
				world.down();
				doEveryMove();
			} else if (input.isKeyPressed(Input.KEY_A) || input.isKeyPressed(Input.KEY_LEFT)) {
				world.left();
				doEveryMove();
			} else if (input.isKeyPressed(Input.KEY_D) || input.isKeyPressed(Input.KEY_RIGHT)) {
				world.right();
				doEveryMove();
			}
		}

		possibleMapEvents(sbg, input);
	}

	private void doEveryMove() {
		wasserbalken.setCurrent(PlayerData.CURRENTWATER--);
		randomenemy = Global.randomNumber(0, enemychance);
		didfirstmove = true;
		world.walkedOn();
	}

	private void possibleMapEvents(StateBasedGame sbg, Input input) {
		// KEIN CURRENTWATER
		if (PlayerData.CURRENTWATER == 0) {
//			todesursachezahl = 1;
			sbg.enterState(State.DEATHSCREEN.id(), Global.fadeOut(), Global.fadeIn());
		} else

		// IN GEBÄUDE GEHEN
		if (world.checkForBuilding()) {
			sbg.enterState(State.FIGHT.id(), Global.fadeOut(), Global.fadeIn());
			world.generatePath();
		} else

		// RANDOM GEGNER
//		if (!world.checkForBuilding() && !world.checkForHome() && randomenemy == enemychance && didfirstmove) {
//			sbg.enterState(State.FIGHT.id(), Global.fadeOut(), Global.fadeIn());
//		} else

		// HOME BETRETEN
		if (world.checkForHome() && didfirstmove) {
			sbg.enterState(State.HOME.id(), Global.fadeOut(), Global.fadeIn());
		} else
		
		// InventoryState oeffnen
		if (input.isKeyPressed(Input.KEY_I)) {
			sbg.enterState(State.INVENTORY.id(), Global.fadeOut(), Global.fadeIn());
		}
	}
}
