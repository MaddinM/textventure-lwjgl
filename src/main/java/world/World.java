package world;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import java.util.ArrayList;

public class World {
	public ArrayList<WorldBlock> worldList;
	private WorldGenerator worldGenerator;
	private WorldMover worldMover;

	public World(ArrayList<WorldBlock> worldList) {
		this.worldList = worldList;
		worldMover = new WorldMover(worldList);
		worldGenerator = new WorldGenerator(worldList);
	}

	public void render(Graphics g) {
		for (WorldBlock worldblock : worldList) {
			worldblock.render(g);
		}

		worldGenerator.render(g);
	}

	public void update(GameContainer gc) {
		worldMover.update(gc);
	}
	
	void generatePath() {
		getBlockPlayerIsOn().forceStreet();
		for(WorldBlock worldblock : worldList) {
			worldblock.setPath();
		}
	}
	
	void walkedOn() {
		getBlockPlayerIsOn().walkedOn();
	}

	private WorldBlock getBlockPlayerIsOn() {
		for (WorldBlock worldblock : worldList) {
			if (worldblock.getX() == worldGenerator.player.getX() && worldblock.getY() == worldGenerator.player.getY()) {
				return worldblock;
			}
		}
		return null;
	}

	boolean checkForBuilding() {
		return getBlockPlayerIsOn().getColor() == Color.black && !checkForHome();
	}

	boolean checkForHome() {
		return getBlockPlayerIsOn().getName().equals("Z");
	}

	void up() {
		worldMover.moveWorldUp();
		worldGenerator.checkWorldAndGenerateTop();
	}

	void down() {
		worldMover.moveWorldDown();
		worldGenerator.checkWorldAndGenerateDown();
	}

	void left() {
		worldMover.moveWorldLeft();
		worldGenerator.checkWorldAndGenerateLeft();
	}

	void right() {
		worldMover.moveWorldRight();
		worldGenerator.checkWorldAndGenerateRight();
	}
}
