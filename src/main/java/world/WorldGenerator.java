package world;

import main.Global;
import org.newdawn.slick.Graphics;

import java.util.ArrayList;

public class WorldGenerator {
	public static String letters = Global.charBar(20, 'T') + Global.charBar(20, ',') + Global.charBar(20, '.') + "HDGEKO";
	private ArrayList<WorldBlock> worldList;
	WorldPlayer player;

	WorldGenerator(ArrayList<WorldBlock> worldList) {
		this.worldList = worldList;
		player = new WorldPlayer();
	}

	public void render(Graphics g) {
		player.render(g);
	}

	void checkWorldAndGenerateTop() {
		checkForBlockRelativeToPlayerAndGenerate(0, -2);
		checkForBlockRelativeToPlayerAndGenerate(-1, -1);
		checkForBlockRelativeToPlayerAndGenerate(1, -1);
		checkForBlockRelativeToPlayerAndGenerate(-2, 0);
		checkForBlockRelativeToPlayerAndGenerate(2, 0);
	}

	void checkWorldAndGenerateDown() {
		checkForBlockRelativeToPlayerAndGenerate(0, 2);
		checkForBlockRelativeToPlayerAndGenerate(-1, 1);
		checkForBlockRelativeToPlayerAndGenerate(1, 1);
		checkForBlockRelativeToPlayerAndGenerate(-2, 0);
		checkForBlockRelativeToPlayerAndGenerate(2, 0);
	}

	void checkWorldAndGenerateLeft() {
		checkForBlockRelativeToPlayerAndGenerate(-2, 0);
		checkForBlockRelativeToPlayerAndGenerate(-1, -1);
		checkForBlockRelativeToPlayerAndGenerate(-1, 1);
		checkForBlockRelativeToPlayerAndGenerate(0, -2);
		checkForBlockRelativeToPlayerAndGenerate(0, 2);
	}

	void checkWorldAndGenerateRight() {
		checkForBlockRelativeToPlayerAndGenerate(2, 0);
		checkForBlockRelativeToPlayerAndGenerate(1, -1);
		checkForBlockRelativeToPlayerAndGenerate(1, 1);
		checkForBlockRelativeToPlayerAndGenerate(0, -2);
		checkForBlockRelativeToPlayerAndGenerate(0, 2);
	}

	// Top = y-2 // Bottom = y+2 // Left = x-2 // Right = x+2 // TopRight = x+1 y-1
	// // TopLeft = x-1 y-1 // BottomRight = x+1 y+1 // BottomLeft = x-1 y+1
	private void checkForBlockRelativeToPlayerAndGenerate(int checkX, int checkY) {
		int onPlayer = 0;
		for (WorldBlock worldBlock : worldList) {
			if (worldBlock.getX() == player.getX() + checkX && worldBlock.getY() == player.getY() + checkY) {
				onPlayer++;
			}
		}

		if (onPlayer == 0) {
			worldList.add(new WorldBlock(Global.randomLetter(letters), player.getX() + checkX, player.getY() + checkY));
		}
	}
}
