package world;

import org.newdawn.slick.GameContainer;

import java.util.ArrayList;

public class WorldMover {
	private ArrayList<WorldBlock> worldList;

	public WorldMover(ArrayList<WorldBlock> worldList) {
		this.worldList = worldList;
	}

	public void update(GameContainer gc) {

	}

	public void moveWorldLeft() {
		for (WorldBlock worldblock : worldList) {
			worldblock.moveLeft();
		}
	}

	public void moveWorldRight() {
		for (WorldBlock worldblock : worldList) {
			worldblock.moveRight();
		}
	}

	public void moveWorldUp() {
		for (WorldBlock worldblock : worldList) {
			worldblock.moveUp();
		}
	}

	public void moveWorldDown() {
		for (WorldBlock worldblock : worldList) {
			worldblock.moveDown();
		}
	}
}
