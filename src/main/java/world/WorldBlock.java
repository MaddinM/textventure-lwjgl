package world;

import main.Coords;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import save.WorldData;

public class WorldBlock {
	private String name;
	private int x;
	private int y;
	private Color c;
	private boolean walkedOn;

	public WorldBlock(String name, int x, int y) {
		this.name = name;
		this.x = x;
		this.y = y;

		if (name.equals(".") || name.equals(",") || name.equals("T")) {
			c = Color.lightGray;
		} else {
			c = Color.black;
		}
	}

	public void render(Graphics g) {
		if (onScreen() && !onPlayer()) {
			g.setColor(c);
			g.drawString(name, Coords.x(x), Coords.y(y));
		}
	}
	
	void walkedOn() {
		walkedOn = true;
	}
	
	void setPath() {
		if(walkedOn) {
			setStreet();
		}
	}

	void moveLeft() {
		x++;
	}

	void moveRight() {
		x--;
	}

	void moveUp() {
		y++;
	}

	void moveDown() {
		y--;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public String getName() {
		return name;
	}

	private void setStreet() {
		if (c == Color.lightGray) {
			name = "#";
		}
	}

	void forceStreet() {
		c = Color.lightGray;
		name = "#";
	}

	public Color getColor() {
		return c;
	}

	private boolean onScreen() {
		return getX() > Coords.maxX() / 3 && getX() < Coords.maxX() - 1 && getY() > 0 && getY() < Coords.maxY() - 1;
	}

	private boolean onPlayer() {
		return getX() == WorldData.playerX && getY() == WorldData.playerY;
	}
}
