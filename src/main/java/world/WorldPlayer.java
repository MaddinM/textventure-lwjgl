package world;

import main.Constants;
import main.Coords;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import save.WorldData;

public class WorldPlayer {
	private int x = WorldData.playerX;
	private int y = WorldData.playerY;

	public void render(Graphics g) {
		g.setFont(Constants.FONT);
		g.setColor(Color.black);
		g.drawString("@", Coords.x(x), Coords.y(y));
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}