package options;

import main.Constants;
import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import java.util.Locale;
import java.util.ResourceBundle;

public class OptionsButton {
    private String string;
    private int x;
    private int y;

    private boolean hover = false;

    public OptionsButton(String string, int x, int y) {
        this.string = string;
        this.x = x;
        this.y = y;
    }

    public void render(Graphics g) {
        if (hover) {
            g.setColor(Color.black);
        } else {
            g.setColor(Color.lightGray);
        }
        Global.drawSpacedString(g, string, x, y);
    }

    public void update(GameContainer gc) {
        Input input = gc.getInput();
        int mouseX = input.getMouseX();
        int mouseY = input.getMouseY();

        hover = (mouseX > Coords.x(x) && mouseX < Coords.x(x + string.length())) &&
                (mouseY > Coords.y(y) && mouseY < Coords.y(y + 1));

        if (hover && input.isMousePressed(0)) {
            if (Locale.getDefault().toLanguageTag().equals("en-US")) {
                Locale.setDefault(new Locale("de", "DE"));
                string = "Sprache ändern";
            } else {
                Locale.setDefault(new Locale("en", "US"));
                string = "Change language";
            }
            Constants.STRINGS = ResourceBundle.getBundle("Strings");
        }
    }
}
