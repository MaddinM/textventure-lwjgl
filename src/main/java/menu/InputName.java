package menu;

import enums.State;
import main.Constants;
import main.Global;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.StateBasedGame;
import save.PlayerData;
import save.WorldData;

public class InputName {
	private String name = "";

	public void render(Graphics g) {
		String nameInput = Constants.STRINGS.getString("name_input");
		int nameInputX = 3;
		int nameInputY = 2;

		Global.drawSpacedString(g, nameInput + ":", nameInputX, nameInputY);
		Global.drawSpacedString(g, name, nameInputX + nameInput.length() + 2, nameInputY);
	}

	public void update(GameContainer gc, StateBasedGame sbg) {
		name += Global.getKeyString(gc);

		if (gc.getInput().isKeyPressed(Input.KEY_BACK) && name.length() > 0) {
			name = name.substring(0, name.length() - 1);
		}

		if (gc.getInput().isKeyPressed(Input.KEY_ENTER) && name.length() > 0) {
			PlayerData.defaultPlayerData(name);
			WorldData.clear();
			WorldData.defaultWorldData();
			sbg.enterState(State.HOME.id(), Global.fadeOut(), Global.fadeIn());
		}
	}
}
