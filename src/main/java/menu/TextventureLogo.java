package menu;

import main.Constants;
import main.Coords;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class TextventureLogo {
	private int x;
	private int y;
	private boolean withBackground;
	private Color color;
	private int scroll;

	public TextventureLogo(int x, int y, boolean withBackground, Color color) {
		this.x = x;
		this.y = y;
		this.withBackground = withBackground;
		this.color = color;
	}
	
	public TextventureLogo(boolean withBackground, Color color) {
		x = (Coords.maxX() - 54) / 2;
		y = Coords.maxY() / 3;
		this.withBackground = withBackground;
		this.color = color;
	}

	public void render(Graphics g) {
		if (withBackground) {
			g.setColor(new Color(226, 226, 226));
			int laengebg = 0;
			for (int j = 0; j < 4; j++) {
				for (int i = 0; i < 52; i++) {
					g.drawString(Constants.TEXTVENTURE_LOGO_BG.substring(laengebg, laengebg + 1), Coords.x(x + i + 1), Coords.y(y + j + 1));
					laengebg++;
				}
			}
		}

		g.setColor(color);
		int laenge = 0;
		for (int j = 0; j < 5; j++) {
			for (int i = 0; i < 54; i++) {
				g.drawString(Constants.TEXTVENTURE_LOGO.substring(laenge, laenge + 1), Coords.x(x + i), Coords.y(y + j) + scroll);
				laenge++;
			}
		}
	}

	public void setScroll(int scroll) {
		this.scroll = scroll;
	}
}