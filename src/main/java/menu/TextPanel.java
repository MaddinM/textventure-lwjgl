package menu;

import main.Audio;
import main.Constants;
import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import java.util.ArrayList;

public class TextPanel {
	private static int lastLinesAdded;
	private static ArrayList<String> textPanel = new ArrayList<>();

	public void render(Graphics g, int bottom) {
		for (int i = 0; i < textPanel.size(); i++) {
			if (i < lastLinesAdded) {
				g.setColor(Color.lightGray);
			} else {
				g.setColor(Global.getGrey(220));
			}

			if (i < bottom) {
				Global.drawSpacedString(g, textPanel.get(i), 1, 1 + i);
			}
		}
	}

	private void addToTextPanel(String s) {
		StringBuilder line = new StringBuilder();
		int count = 0;
		int countlines = 0;
		int width = Coords.maxX() / 3 - 1;

		if (s.length() <= width) {
			textPanel.add(s);
			lastLinesAdded = 1;
			return;
		}

		String[] strArr = s.split(" ");
		for (String value : strArr) {
			if (count + value.length() <= width) {
				count += value.length() + 1;
				line.append(value).append(" ");
			} else {
				textPanel.add(countlines, line.toString());
				countlines++;
				count = value.length() + 1;
				line = new StringBuilder(value + " ");
			}
		}
		textPanel.add(countlines, line.toString());
		countlines++;
		lastLinesAdded = countlines;
	}

	public void addStringArray(String[] text, int index) {
		if (index < text.length) {
			Audio.down.play(1.0f, Constants.VOLUME_SOUND);
			addToTextPanel(text[index]);
		}
	}
	
	public void clear() {
		textPanel.clear();
	}
}
