package menu;

import enums.State;
import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.StateBasedGame;

public class MenuButton {
	private String string;
	private int x;
	private int y;
	private State state;

	private boolean hover = false;

	public MenuButton(String string, int x, int y, State state) {
		this.string = string;
		this.x = x;
		this.y = y;
		this.state = state;
	}

	public void render(Graphics g) {
		if (hover) {
			g.setColor(Color.black);
		} else {
			g.setColor(Color.lightGray);
		}
		Global.drawSpacedString(g, string, x, y);
	}

	public void update(GameContainer gc, StateBasedGame sbg) {
		Input input = gc.getInput();
		int mouseX = input.getMouseX();
		int mouseY = input.getMouseY();

		hover = (mouseX > Coords.x(x) && mouseX < Coords.x(x + string.length())) &&
				(mouseY > Coords.y(y) && mouseY < Coords.y(y + 1));

		if (hover && input.isMouseButtonDown(0)) {
			if (state != null) {
				sbg.enterState(state.id(), Global.fadeOut(), Global.fadeIn());
			} else {
				System.exit(0);
			}
		}
	}
}
