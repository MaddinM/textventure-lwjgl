package states;

import enums.State;
import ingame.Bar;
import main.*;
import menu.TextPanel;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import save.InventoryData;
import save.PlayerData;
import save.WorldData;
import world.World;
import world.WorldController;

/**
 * Der State, wenn man sich draußen, also auf der Karte befindet.
 * 
 * @author Maddin
 */
public class IngameState extends BasicGameState {
	private State state;
	public static int todesursachezahl; // TODO enum einrichten
	private Border border;
	private World world;
	private Bar lebensbalken;
	private Bar wasserbalken;
	private Bar inventarbalken;
	private WorldController worldcontroller;
	private TextPanel textpanel;

	private int tutorialTextIndex = 0;
	
	/**
	 * Der Konstruktor gibt an um welchen State es sich handelt.
	 * 
	 * @param state Der State in dem man sich gerade befindet wird an die Klasse
	 *              weitergegeben.
	 */
	public IngameState(State state) {
		this.state = state;
	}

	/**
	 * Wird ausgeführt, wenn State betreten wird. Spielt die Musik für diesen State
	 * ab. Setzt die Welt und den Wasserbalken zurück
	 */
	public void enter(GameContainer gc, StateBasedGame sbg) {
		PlayerData.CURRENTHEALTH = PlayerData.MAXHEALTH;
		Audio.draussen.loop(1.0f, Constants.VOLUME);

		border = new Border(Coords.maxX() / 3);
		world = new World(WorldData.get());
		lebensbalken = new Bar("L", 1, Coords.maxY() - 4, PlayerData.MAXHEALTH, PlayerData.CURRENTHEALTH, Coords.maxX() / 3 - 4);
		wasserbalken = new Bar("W", 1, Coords.maxY() - 3, PlayerData.MAXWATER, PlayerData.CURRENTWATER, Coords.maxX() / 3 - 4);
		inventarbalken = new Bar("I", 1, Coords.maxY() - 2, PlayerData.MAXINVENTORYSPACE, PlayerData.CURRENTINVENTORYUSED, Coords.maxX() / 3 - 4);
		worldcontroller = new WorldController(wasserbalken, world);
		textpanel = new TextPanel();
	}

	/**
	 * Wird ausgeführt, wenn State verlassen wird.
	 */
	public void leave(GameContainer gc, StateBasedGame sbg) {
		WorldData.set(world.worldList);

		worldcontroller = null;
		border = null;
		world = null;
		lebensbalken = null;
		wasserbalken = null;
		inventarbalken = null;
		textpanel = null;

		gc.getInput().clearKeyPressedRecord();
	}

	/**
	 * Wird ausgeführt, wenn State initialisiert wird (in Game-Klasse).
	 */
	public void init(GameContainer gc, StateBasedGame sbg) {
		InventoryData.createFirstItems();
	}

	/**
	 * Hintergrund und Font werden eingestellt. Alles andere wird auf den Bildschirm
	 * gezeichnet.
	 */
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.white);
		g.setFont(Constants.FONT);

		border.render(g);
		world.render(g);
		lebensbalken.render(g);
		wasserbalken.render(g);
		inventarbalken.render(g);
		textpanel.render(g, Coords.maxY() - 5);
	}

	/**
	 * Bewegung des Spielers wird gesteuert Welt wird akualisiert Je nach dem was
	 * der Spieler macht, wird der richtige State aufgerufen
	 */
	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		world.update(gc);
		worldcontroller.update(gc, sbg);
		
		if(gc.getInput().isMousePressed(0)) {
			textpanel.addStringArray(
					Global.getStringArray(Constants.STRINGS.getString("ingame_tutorial_text")),
					tutorialTextIndex++);
		}
	}

	/**
	 * Gibt den State, in dem man ist zurück. Ist in einem BasicGameState Pflicht.
	 */
	public int getID() {
		return state.id();
	}
}
