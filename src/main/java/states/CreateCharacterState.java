package states;

import enums.State;
import main.Border;
import main.Constants;
import menu.InputName;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class CreateCharacterState extends BasicGameState {

	private State state;
	private Border menuBorder;
	private InputName inputName;

	public CreateCharacterState(State state) {
		this.state = state;
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		menuBorder = new Border(0, 95);
		inputName = new InputName();
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		menuBorder = null;
		inputName = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.white);
		g.setFont(Constants.FONT);
		g.setColor(Color.lightGray);

		inputName.render(g);
		menuBorder.render(g);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		inputName.update(gc, sbg);
	}

	public int getID() {
		return state.id();
	}
}
