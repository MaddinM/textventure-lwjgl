package states;

import enums.State;
import loadingscreen.LoadingScreenTextventureLogo;
import main.Constants;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import javax.swing.*;

public class OpenScreenState extends BasicGameState {
	private State state;
	private int timer;
	private LoadingScreenTextventureLogo loadingScreenTextventureLogo;
	private Music openMusic;

	public OpenScreenState(State state) {
		this.state = state;
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		loadingScreenTextventureLogo = new LoadingScreenTextventureLogo();
		try {
			openMusic = new Music(Constants.RES_FOLDER + "music/open.ogg");
			openMusic.play(1.0f, Constants.VOLUME);
		} catch (SlickException e) {
			JOptionPane.showMessageDialog(null, e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		loadingScreenTextventureLogo = null;
		openMusic.stop();
		openMusic = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.black);
		g.setFont(Constants.FONT);

		loadingScreenTextventureLogo.render(g);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		timer++;
		if (timer == 60 * 7 || gc.getInput().isMouseButtonDown(0) || gc.getInput().isKeyDown(Input.KEY_ESCAPE) || gc.getInput().isKeyDown(Input.KEY_ENTER)) {
			sbg.enterState(State.LOADINGSCREEN.id());
		}
		loadingScreenTextventureLogo.update();
	}

	public int getID() {
		return state.id();
	}
}