package states;

import enums.State;
import main.Border;
import main.Constants;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import save.PlayerData;
import save.SaveButton;

public class SaveState extends BasicGameState {
	private State state;
	private Border border;
	private SaveButton saveButtonYes;
	private SaveButton saveButtonNo;

	public SaveState(State state) {
		this.state = state;
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		border = new Border();

		saveButtonYes = new SaveButton(
				PlayerData.NAME,
				Constants.STRINGS.getString("yes"),
				25,
				12);

		saveButtonNo = new SaveButton(
				PlayerData.NAME,
				Constants.STRINGS.getString("no"),
				35,
				12);
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		border = null;
		saveButtonYes = null;
		saveButtonNo = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.white);
		g.setFont(Constants.FONT);

		saveButtonYes.render(g);
		saveButtonNo.render(g);

		g.setColor(Color.lightGray);

		Global.drawSpacedString(g, Constants.STRINGS.getString("save_question"), 12, 5);
		Global.drawSpacedString(g, Constants.STRINGS.getString("save_question_warning"), 5, 6);

		border.render(g);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		saveButtonYes.update(gc, sbg);
		saveButtonNo.update(gc, sbg);
	}

	public int getID() {
		return state.id();
	}
}
