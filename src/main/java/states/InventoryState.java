package states;

import enums.State;
import ingame.Bar;
import inventory.InventoryBackground;
import inventory.InventoryEquipment;
import inventory.ListInventory;
import main.*;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import save.PlayerData;

public class InventoryState extends BasicGameState {
	private State state;
	private InventoryBackground inventarbackground;
	private Border border;
	private ListInventory inventarlist;
	private InventoryEquipment iekopf;
	private InventoryEquipment ieoberkoerper;
	private InventoryEquipment iehose;
	private InventoryEquipment ieschuhe;
	private InventoryEquipment iewaffe;
	private InventoryEquipment iewaffe2;
	private InventoryEquipment iebeutel;
	private InventoryEquipment iewasser;

	private Bar invlebensbalken;
	private Bar invwasserbalken;
	private Bar invinventarbalken;

	public InventoryState(State state) {
		this.state = state;
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		Audio.menu.loop(1.0f, Constants.VOLUME);

		invlebensbalken = new Bar("L", 20, 3, PlayerData.MAXHEALTH, PlayerData.CURRENTHEALTH, 8);
		invwasserbalken = new Bar("W", 20, 4, PlayerData.MAXWATER, PlayerData.CURRENTWATER, 8);
		invinventarbalken = new Bar("I", 20, 5, PlayerData.MAXINVENTORYSPACE, PlayerData.CURRENTINVENTORYUSED, 8);
		inventarbackground = new InventoryBackground();
		border = new Border(Coords.maxX() / 3);
		inventarlist = new ListInventory();
		initInventarSlots();
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		invlebensbalken = null;
		invwasserbalken = null;
		invinventarbalken = null;
		inventarbackground = null;
		border = null;
		inventarlist = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.white);
		g.setFont(Constants.FONT);

		inventarbackground.render(g);
		inventarlist.render(g);
		border.render(g);
		invlebensbalken.render(g);
		invwasserbalken.render(g);
		invinventarbalken.render(g);
		renderInventarSlots(g);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		inventarlist.update(gc, sbg);

		Input input = gc.getInput();
		if (input.isKeyPressed(Input.KEY_ESCAPE)) {
			sbg.enterState(State.INGAME.id(), Global.fadeOut(), Global.fadeIn());
		}
	}

	public int getID() {
		return state.id();
	}

	public void initInventarSlots() {
		iekopf = new InventoryEquipment(24, 7);
		ieoberkoerper = new InventoryEquipment(24, 11);
		iehose = new InventoryEquipment(24, 15);
		ieschuhe = new InventoryEquipment(24, 19);
		iewaffe = new InventoryEquipment(20, 11);
		iewaffe2 = new InventoryEquipment(28, 11);
		iebeutel = new InventoryEquipment(20, 15);
		iewasser = new InventoryEquipment(28, 15);
	}

	public void renderInventarSlots(Graphics g) {
		iekopf.render(g);
		ieoberkoerper.render(g);
		iehose.render(g);
		ieschuhe.render(g);
		iewaffe.render(g);
		iewaffe2.render(g);
		iebeutel.render(g);
		iewasser.render(g);
	}
}
