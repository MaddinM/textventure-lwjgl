package states;

import enums.State;
import load.ListSaveFiles;
import load.LoadBackground;
import main.Border;
import main.Constants;
import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.io.File;

public class LoadSavesState extends BasicGameState {
	private State state;
	private Border border;
	private LoadBackground loadBackground;
	private ListSaveFiles listSaveFiles;

	public LoadSavesState(State state) {
		this.state = state;
		new File("profiles").mkdir();
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		border = new Border(Coords.maxX() / 3);
		loadBackground = new LoadBackground();
		listSaveFiles = new ListSaveFiles();
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		border = null;
		loadBackground = null;
		listSaveFiles = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.white);
		g.setFont(Constants.FONT);

		border.render(g);
		loadBackground.render(g);
		listSaveFiles.render(g);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		listSaveFiles.update(gc, sbg);

		if (gc.getInput().isKeyPressed(Input.KEY_ESCAPE)) {
			sbg.enterState(State.MENU.id(), Global.fadeOut(), Global.fadeIn());
		}
	}

	public int getID() {
		return state.id();
	}
}