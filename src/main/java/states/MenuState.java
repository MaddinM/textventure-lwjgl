package states;

import enums.State;
import main.*;
import menu.MenuButton;
import menu.TextventureLogo;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class MenuState extends BasicGameState {
	private State state;
	private Border menuBorder;
	private TextventureLogo txvLogo;

	private MenuButton newGameButton;
	private MenuButton loadGameMenu;
	private MenuButton optionsButton;
	private MenuButton creditsButton;
	private MenuButton quitGameButton;

	public MenuState(State state) {
		this.state = state;
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		Mouse.setGrabbed(false);

		if (!Audio.menu.playing()) {
			Audio.menu.loop(1.0f, Constants.VOLUME);
		}

		menuBorder = new Border();
		txvLogo = new TextventureLogo((Coords.maxX() - 54) / 2, 3, true, Color.black);

		int buttonsXCoord = 6;
		int buttonsYCoord = 12;

		newGameButton = new MenuButton(
				Constants.STRINGS.getString("new_game_button"),
				buttonsXCoord,
				buttonsYCoord,
				State.CREATECHARACTER);

		loadGameMenu = new MenuButton(
				Constants.STRINGS.getString("load_game_button"),
				buttonsXCoord,
				buttonsYCoord + 2,
				State.LOADSAVES);

		optionsButton = new MenuButton(
				Constants.STRINGS.getString("options_button"),
				buttonsXCoord,
				buttonsYCoord + 4,
				State.OPTIONS);

		creditsButton = new MenuButton(
				Constants.STRINGS.getString("credits_button"),
				buttonsXCoord,
				buttonsYCoord + 6,
				State.CREDITS);

		quitGameButton = new MenuButton(
				Constants.STRINGS.getString("quit_game_button"),
				buttonsXCoord,
				buttonsYCoord + 8,
				null);
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		menuBorder = null;
		txvLogo = null;
		newGameButton = null;
		loadGameMenu = null;
		optionsButton = null;
		creditsButton = null;
		quitGameButton = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.white);
		g.setFont(Constants.FONT);
		g.setColor(Color.lightGray);
		Global.drawSpacedString(g, Constants.VERSION, 68, 9);

		menuBorder.render(g);
		txvLogo.render(g);
		newGameButton.render(g);
		loadGameMenu.render(g);
		optionsButton.render(g);
		creditsButton.render(g);
		quitGameButton.render(g);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		newGameButton.update(gc, sbg);
		loadGameMenu.update(gc, sbg);
		optionsButton.update(gc, sbg);
		creditsButton.update(gc, sbg);
		quitGameButton.update(gc, sbg);
	}

	public int getID() {
		return state.id();
	}
}
