package states;

import enums.State;
import lootscreen.LootScreenGrid;
import lootscreen.LootScreenTimerBar;
import main.*;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class LootScreenState extends BasicGameState {
	private State state;
	private Border border;
	private LootScreenTimerBar lootScreenTimerBar;
	private LootScreenGrid lootScreenGrid;

	public LootScreenState(State state) {
		this.state = state;
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		Audio.looten.loop(1.0f, Constants.VOLUME);
		lootScreenTimerBar = new LootScreenTimerBar(Coords.maxX() / 3 + 1, 1, 10000); // 10000
		lootScreenGrid = new LootScreenGrid(Coords.maxX() / 3 + 1, 2);
		border = new Border(Coords.maxX() / 3);
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		lootScreenTimerBar = null;
		lootScreenGrid = null;
		border = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.white);
		g.setFont(Constants.FONT);

		border.render(g);
		lootScreenTimerBar.render(g);
		lootScreenGrid.render(g);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		Input input = gc.getInput();

		lootScreenTimerBar.update(delta);
		lootScreenGrid.update(gc);

		if (input.isMouseButtonDown(0)) {
			lootScreenTimerBar.stopTimer();
		}

		if (lootScreenTimerBar.time <= 0) {
			lootScreenGrid.clicked = true;
		}

		if (gc.getInput().isKeyPressed(Input.KEY_ESCAPE)) {
			sbg.enterState(State.INGAME.id(), Global.fadeOut(), Global.fadeIn());
		}
	}

	public int getID() {
		return state.id();
	}
}
