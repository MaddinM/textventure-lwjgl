package states;

import enums.State;
import main.*;
import options.OptionsButton;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import javax.swing.*;

public class OptionsState extends BasicGameState {
	private State state;
	private Border border;

	private OptionsButton languageButton;

	public OptionsState(State state) {
		this.state = state;
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		languageButton = new OptionsButton(
				Constants.STRINGS.getString("change_language"),
				6,
				12
		);
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		languageButton = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
		border = new Border();
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.white);
		g.setFont(Constants.FONT);

		languageButton.render(g);

		border.render(g);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		Input input = gc.getInput();
		if (input.isMouseButtonDown(1)) {
			sbg.enterState(State.MENU.id(), Global.fadeOut(), Global.fadeIn());
		}

		/**if (input.isMouseButtonDown(1)) {
			setFullscreen();
		}*/

		languageButton.update(gc);
	}

	private void setFullscreen() {
		//FULLSCREEN = !FULLSCREEN;
		try {
			Constants.WINDOW_WIDTH = 1280;
			Constants.WINDOW_HEIGHT = 720;
			Textventure.appgc.setDisplayMode(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT, Constants.FULLSCREEN);
			Constants.FONT = new TrueTypeFont(new java.awt.Font("Courier New", java.awt.Font.BOLD, Constants.WINDOW_WIDTH / 60), true);
		} catch (SlickException e) {
			JOptionPane.showMessageDialog(null, e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public int getID() {
		return state.id();
	}
}