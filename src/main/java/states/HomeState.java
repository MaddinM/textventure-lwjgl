package states;

import enums.State;
import main.Border;
import main.Constants;
import main.Coords;
import main.Global;
import menu.MenuButton;
import menu.TextPanel;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import save.PlayerData;

public class HomeState extends BasicGameState {
	private State state;
	private Border border;
	private MenuButton hausverlassenbutton;
	private MenuButton speichernbutton;
	private MenuButton zurueckbutton;
	private TextPanel textpanel;

	private int tutorialTextIndex = 0;

	public HomeState(State state) {
		this.state = state;
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		PlayerData.CURRENTWATER = PlayerData.MAXWATER;

		border = new Border(Coords.maxX() / 3);
		hausverlassenbutton = new MenuButton("Haus verlassen", Coords.maxX() / 3 + 2, 2, State.INGAME);
		speichernbutton = new MenuButton("Speichern", Coords.maxX() / 3 + 2, 4, State.SAVE);
		zurueckbutton = new MenuButton("Menu", Coords.maxX() / 3 + 2, 6, State.MENU);
		textpanel = new TextPanel();
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		border = null;
		hausverlassenbutton = null;
		speichernbutton = null;
		zurueckbutton = null;
		textpanel = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.white);
		g.setFont(Constants.FONT);

		border.render(g);
		hausverlassenbutton.render(g);
		speichernbutton.render(g);
		zurueckbutton.render(g);
		textpanel.render(g, Coords.maxY() - 2);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		hausverlassenbutton.update(gc, sbg);
		speichernbutton.update(gc, sbg);
		zurueckbutton.update(gc, sbg);

		if (gc.getInput().isMousePressed(0)) {
			textpanel.addStringArray(
					Global.getStringArray(Constants.STRINGS.getString("home_tutorial_text")),
					tutorialTextIndex++);
		}
	}

	public int getID() {
		return state.id();
	}

}