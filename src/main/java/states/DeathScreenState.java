package states;

import enums.CauseOfDeath;
import enums.State;
import loadingscreen.GameOverLogo;
import main.Constants;
import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class DeathScreenState extends BasicGameState {
	private State state;
	private int timer;
	private GameOverLogo gameOverLogo;
	public static CauseOfDeath causeOfDeath;

	public DeathScreenState(State state) {
		this.state = state;
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		gameOverLogo = new GameOverLogo();
		timer = 0;
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		gameOverLogo = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.black);
		g.setFont(Constants.FONT);
		g.setColor(Color.white);

		gameOverLogo.render(g);

		if (timer > 300) {
			Global.drawSpacedString(g, deathMessage(causeOfDeath), (Coords.maxX() - deathMessage(causeOfDeath).length()) / 2, 18);
		}

		String backmessage = Constants.STRINGS.getString("death_screen_back_message");
		if (timer > 500) {
			Global.drawSpacedString(g, backmessage, (Coords.maxX() - backmessage.length()) / 2, 22);
		}
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		gameOverLogo.update();
		timer++;

		if (timer > 500) {
			Input input = gc.getInput();
			if (input.isKeyPressed(Input.KEY_SPACE)) {
				sbg.enterState(State.MENU.id(), Global.fadeOut(), Global.fadeIn());
			}
		}
	}

	public int getID() {
		return state.id();
	}

	private String deathMessage(CauseOfDeath ursache) {
		switch (ursache) {
		case DIED_OF_THIRST:
			return CauseOfDeath.DIED_OF_THIRST.getDeathMessage();
		case KILLED_IN_FIGHT:
			return CauseOfDeath.KILLED_IN_FIGHT.getDeathMessage();
		default:
			return Constants.STRINGS.getString("error");
		}
	}
}
