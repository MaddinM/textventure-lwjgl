package states;

import enums.State;
import loadingscreen.LoadingScreenBar;
import main.Audio;
import main.Constants;
import main.Global;
import menu.TextventureLogo;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.awt.*;

public class LoadingScreenState extends BasicGameState {
	private State state;
	private Audio audio;
	private TextventureLogo textventureLogo;
	private LoadingScreenBar loadingScreenBar;

	public LoadingScreenState(State state) {
		this.state = state;
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		audio = new Audio();
		textventureLogo = new TextventureLogo(false, Color.white);
		loadingScreenBar = new LoadingScreenBar();
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		audio = null;
		textventureLogo = null;
		loadingScreenBar = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
		Constants.FONT = new TrueTypeFont(new Font("Courier New", Font.BOLD, Constants.WINDOW_WIDTH / 60), true);
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.black);
		g.setFont(Constants.FONT);
		Global.drawSpacedString(g, Constants.VERSION, 68, 16);

		textventureLogo.render(g);
		loadingScreenBar.render(g);

		audio.render(g);
		loadingScreenBar.setLoaded(audio.amountLoadedFiles);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		if (audio.finishedLoading) {
			sbg.enterState(State.MENU.id(), Global.fadeOut(), Global.fadeIn());
		}
	}

	public int getID() {
		return state.id();
	}
}
