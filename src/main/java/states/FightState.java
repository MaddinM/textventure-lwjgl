package states;

import enums.CauseOfDeath;
import enums.Enemy;
import enums.State;
import fight.FightAttacks;
import fight.FightBackground;
import fight.FightEnemy;
import ingame.Bar;
import main.*;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import save.PlayerData;

public class FightState extends BasicGameState {
	private State state;
	private FightAttacks kampfattacken;
	private FightBackground kampfbackground;
	private Border border;
	private FightEnemy gegner;
	private Bar lebenspieler;
	private Bar lebengegner;

	public FightState(State state) {
		this.state = state;
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		gegner = new FightEnemy(Enemy.getRandomEnemy(), PlayerData.LEVEL);
		kampfattacken = new FightAttacks(gegner, this);
		kampfbackground = new FightBackground(gegner);
		border = new Border(Coords.maxX() / 3, Coords.maxX() / 3 * 2 - 2, Coords.maxX() / 3 * 2 + 1);
		lebenspieler = new Bar("L", Coords.maxX() / 3 + 1, 18, PlayerData.MAXHEALTH, PlayerData.CURRENTHEALTH, 17);
		lebengegner = new Bar("L", Coords.maxX() / 3 * 2 + 2, 18, gegner.getMaxLeben(), gegner.getLeben(), 17);

		Audio.kampf.loop(1.0f, Constants.VOLUME);
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		kampfattacken = null;
		kampfbackground = null;
		border = null;
		lebenspieler = null;
		lebengegner = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setFont(Constants.FONT);

		kampfattacken.render(g);
		kampfbackground.render(g);
		border.render(g);
		lebenspieler.render(g);
		lebengegner.render(g);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		kampfattacken.update(gc, delta);

		if (lebenspieler.isEmpty()) {
			DeathScreenState.causeOfDeath = CauseOfDeath.KILLED_IN_FIGHT;
			sbg.enterState(State.DEATHSCREEN.id(), Global.fadeOut(), Global.fadeIn());
		}

		if (lebengegner.isEmpty()) {
			sbg.enterState(State.LOOTSCREEN.id(), Global.fadeOut(), Global.fadeIn());
		}
	}

	public int getID() {
		return state.id();
	}

	public void getDamage(int damage) {
		PlayerData.CURRENTHEALTH -= damage;
		lebenspieler.setCurrent(PlayerData.CURRENTHEALTH);
	}

	public void makeDamage(int damage) {
		gegner.setLeben(-damage);
		lebengegner.setCurrent(gegner.getLeben());
	}
}
