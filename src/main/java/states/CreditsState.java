package states;

import enums.State;
import main.Audio;
import main.Constants;
import main.Global;
import menu.TextventureLogo;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class CreditsState extends BasicGameState {

	private State state;
	private TextventureLogo textventureLogo;
	private int scroll;

	public CreditsState(State state) {
		this.state = state;
	}

	public void enter(GameContainer gc, StateBasedGame sbg) {
		Mouse.setGrabbed(true);

		scroll = 0;
		Audio.credits.loop(1.0f, Constants.VOLUME);
		textventureLogo = new TextventureLogo(5, 24, false, Color.white);
	}

	public void leave(GameContainer gc, StateBasedGame sbg) {
		textventureLogo = null;

		gc.getInput().clearKeyPressedRecord();
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setBackground(Color.black);
		g.setFont(Constants.FONT);

		textventureLogo.render(g);

		Global.drawSpacedString(g, Constants.STRINGS.getString("creator_title"), 7, 33, scroll, Color.gray);
		Global.drawSpacedString(g, Constants.STRINGS.getString("creator"), 7, 34, scroll, Color.white);

		Global.drawSpacedString(g, Constants.STRINGS.getString("testers_title"), 7, 36, scroll, Color.gray);
		Global.drawSpacedString(g, Constants.STRINGS.getString("testers"), 7, 37, scroll, Color.white);

		Global.drawSpacedString(g, Constants.STRINGS.getString("music_by_title"), 7, 39, scroll, Color.gray);
		Global.drawSpacedString(g, Constants.STRINGS.getString("music_by"), 7, 40, scroll, Color.white);

		Global.drawSpacedString(g, Constants.STRINGS.getString("powered_by_title"), 7, 42, scroll, Color.gray);
		Global.drawSpacedString(g, Constants.STRINGS.getString("powered_by"), 7, 43, scroll, Color.white);

		Global.drawSpacedString(g, Constants.STRINGS.getString("inspired_by_title"), 7, 45, scroll, Color.gray);
		Global.drawSpacedString(g, Constants.STRINGS.getString("inspired_by"), 7, 46, scroll, Color.white);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		textventureLogo.setScroll(scroll--);

		Input input = gc.getInput();
		if (input.isMouseButtonDown(0) || scroll < -2000) {
			sbg.enterState(State.MENU.id(), Global.fadeOut(), Global.fadeIn());
		}
	}

	public int getID() {
		return state.id();
	}
}
