package loadingscreen;

import main.Coords;
import org.newdawn.slick.Graphics;

import java.util.ArrayList;

public class GameOverLogo {
	private String gameover = "  ____                         ___                  / ___| __ _ _ __ ___   ___"
			+ "   / _ \\__   _____ _ __ | |  _ / _` | '_ ` _ \\ / _ \\ | | | \\ \\ / / _ \\ '__|| |_| | (_| |"
			+ " | | | | |  __/ | |_| |\\ V /  __/ |    \\____|\\__,_|_| |_| |_|\\___|  \\___/  \\_/ \\___|_|   ";
	private int breite = 51;
	private int hoehe = 5;
	private ArrayList<LoadingScreenLetter> gameoverlist = new ArrayList<>();

	public GameOverLogo() {
		int offsetX = (Coords.maxX() - breite) / 2;
		int offsetY = Coords.maxY() / 3;
		
		for (int x = 0; x < hoehe; x++) {
			for (int y = 0; y < breite; y++) {
				gameoverlist.add(new LoadingScreenLetter(Coords.x(y + offsetX), Coords.y(x + offsetY), gameover.substring(x * breite + y, x * breite + y + 1)));
			}
		}
	}

	public void render(Graphics g) {
		for (LoadingScreenLetter l : gameoverlist) {
			l.render(g);
		}
	}

	public void update() {
		for (LoadingScreenLetter l : gameoverlist) {
			l.update();
		}
	}
}
