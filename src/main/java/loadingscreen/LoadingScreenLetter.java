package loadingscreen;

import main.Global;
import org.newdawn.slick.Graphics;

import java.util.concurrent.ThreadLocalRandom;

public class LoadingScreenLetter {
	private int timer;
	private int speed = 10;
	private int nextlettertimer;
	private String anzeige = " ";
	private int anzahlblank = ThreadLocalRandom.current().nextInt(0, 15) * speed;
	private int anzahlchange = ThreadLocalRandom.current().nextInt(15, 30) * speed;
	private int x;
	private int y;
	private String letzterbuchstabe;

	public LoadingScreenLetter(int x, int y, String letzterbuchstabe) {
		this.x = x;
		this.y = y;
		this.letzterbuchstabe = letzterbuchstabe;
	}

	public void update() {
		timer++;
		nextlettertimer++;

		if (timer <= anzahlblank) {
			anzeige = " ";
		} else if (timer >= anzahlchange) {
			anzeige = letzterbuchstabe;
		} else if (nextlettertimer >= speed) {
			anzeige = Global.randomLetter();
			nextlettertimer = 0;
		}
	}

	public void render(Graphics g) {
		g.drawString(anzeige, x, y);
	}
}