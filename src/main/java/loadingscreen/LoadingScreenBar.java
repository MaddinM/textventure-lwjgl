package loadingscreen;

import main.Constants;
import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class LoadingScreenBar {
	private int finishedloading;

	public void render(Graphics g) {
		g.setColor(new Color(30, 30, 30));
		Global.drawSpacedString(g, "####################", (Coords.maxX() - 20) / 2, 18);

		g.setColor(Color.white);
		Global.drawSpacedString(g, new String(new char[(int) scaleToBar(finishedloading)]).replace('\0', '#'), (Coords.maxX() - 20) / 2, 18);
	}

	public void setLoaded(int finishedloading) {
		this.finishedloading = finishedloading;
	}

	private double scaleToBar(int finishedloading) {
		double d = 20.0 / Constants.AMOUNT_FILES_TO_LOAD * finishedloading;
		return d;
	}
}