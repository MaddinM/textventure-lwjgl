package loadingscreen;

import main.Constants;
import main.Coords;
import org.newdawn.slick.Graphics;

import java.util.ArrayList;

public class LoadingScreenTextventureLogo {
	private int breite = 54;
	private int hoehe = 5;

	private ArrayList<LoadingScreenLetter> lsllist = new ArrayList<LoadingScreenLetter>();
	private ArrayList<LoadingScreenLetter> lsllistversion = new ArrayList<LoadingScreenLetter>();

	public LoadingScreenTextventureLogo() {
		int offsetX = (Coords.maxX() - breite) / 2;
		int offsetY = Coords.maxY() / 3;
		
		// ArrayList für Version
		for (int x = 0; x < Constants.VERSION.length(); x++) {
			lsllistversion.add(new LoadingScreenLetter(Coords.x(x + 68), Coords.y(16), Constants.VERSION.substring(x, x + 1)));
		}

		// ArrayList für Logo
		for (int y = 0; y < hoehe; y++) {
			for (int x = 0; x < breite; x++) {
				lsllist.add(new LoadingScreenLetter(Coords.x(x + offsetX), Coords.y(y + offsetY),
						Constants.TEXTVENTURE_LOGO.substring(y * breite + x, y * breite + x + 1)));
			}
		}
	}

	public void update() {
		for (LoadingScreenLetter l : lsllist) {
			l.update();
		}

		for (LoadingScreenLetter l : lsllistversion) {
			l.update();
		}
	}

	public void render(Graphics g) {
		for (LoadingScreenLetter l : lsllist) {
			l.render(g);
		}

		for (LoadingScreenLetter l : lsllistversion) {
			l.render(g);
		}
	}
}
