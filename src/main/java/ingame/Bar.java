package ingame;

import main.Coords;
import main.Global;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import java.nio.CharBuffer;

public class Bar {
	private String letterInFrontOfBar;
	private int x;
	private int y;
	private double maxBar;
	private double currentBar;
	private int barLength;

	public Bar(String letterInFrontOfBar, int x, int y, double maxBar, double currentBar, int barLength) {
		this.letterInFrontOfBar = letterInFrontOfBar;
		this.x = x;
		this.y = y;
		this.maxBar = maxBar;
		this.currentBar = currentBar;
		this.barLength = barLength;
	}

	public void render(Graphics g) {
		g.setColor(Color.lightGray);
		g.drawString(letterInFrontOfBar, Coords.x(x), Coords.y(y));

		g.setColor(Color.black);
		g.drawString("[", Coords.x(x + 1), Coords.y(y));
		g.drawString("]", Coords.x(x + barLength + 2), Coords.y(y));

		g.setColor(new Color(226, 226, 226));
		Global.drawSpacedString(g, currentBar(barLength), x + 2, y);

		g.setColor(Color.gray);
		Global.drawSpacedString(g, currentBar(currentBarLength()), x + 2, y);
	}

	public int currentBarLength() {
		double prozent = currentBar / maxBar;
		double anzahlBalken = prozent * barLength;
		return (int) anzahlBalken;
	}

	public String currentBar(int laenge) {
		return CharBuffer.allocate(laenge).toString().replace('\0', '#');
	}

	public void current(double x) {
		if (currentBar + x < 0) {
			currentBar = 0;
		} else if (currentBar + x > maxBar) {
			currentBar = maxBar;
		} else {
			currentBar += x;
		}
	}

	public double getCurrent() {
		return currentBar;
	}

	public double getMax() {
		return maxBar;
	}

	public void setCurrent(double newcurrent) {
		if (newcurrent > maxBar) {
			currentBar = maxBar;
		} else if (newcurrent < 0) {
			currentBar = 0;
		} else {
			currentBar = newcurrent;
		}
	}

	public boolean isEmpty() {
		return currentBar == 0;
	}
}
